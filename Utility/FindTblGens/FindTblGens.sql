--------------- Manage temp tables =>
IF object_id('tempdb..#tblRelation') is not null
    DROP TABLE #tblRelation;
IF object_id('tempdb..#tblDetail') is not null
    DROP TABLE #tblDetail;


--------------- Table relations detail =>
CREATE TABLE #tblRelation (ObjectId VARCHAR(100) NOT NULL,
							Name VARCHAR(100) NOT NULL,
							[Schema] VARCHAR(100) NOT NULL,
							[Column] VARCHAR(100) NOT NULL,
							FkFromObjectId VARCHAR(100) NOT NULL,							
							FkFromTbl VARCHAR(100) NOT NULL,
							FkFromSchema VARCHAR(100) NOT NULL,
							FkFromClm VARCHAR(100) NOT NULL)
INSERT
	INTO #tblRelation
	SELECT
		parent_object_id,
		OBJECT_NAME(parent_object_id),
		OBJECT_SCHEMA_NAME(parent_object_id),
		c.NAME,
		referenced_object_id,
		OBJECT_NAME(referenced_object_id),
		OBJECT_SCHEMA_NAME(referenced_object_id),
		cref.NAME
	FROM 
		sys.foreign_key_columns fk
	INNER JOIN 
		sys.columns c 
		   ON fk.parent_column_id = c.column_id 
			  AND fk.parent_object_id = c.object_id
	INNER JOIN 
		sys.columns cref 
		   ON fk.referenced_column_id = cref.column_id 
			  AND fk.referenced_object_id = cref.object_id
SELECT *
	FROM #tblRelation
	ORDER BY Name, [Schema], FkFromTbl, FkFromSchema




---------- Find first generation tables and schema detail for all table =>
CREATE TABLE #tblDetail (Name VARCHAR(100) NOT NULL,
						 [Schema] VARCHAR(100) NOT NULL,
						 Generation INT NULL)
INSERT 
	INTO #tblDetail(Name, [Schema], Generation)
	(SELECT 
		TABLE_NAME, 
		TABLE_SCHEMA,
		(CASE 
			WHEN(
			/*if tbl has no fk, first generation tbl, 0*/
			(SELECT COUNT(*) 
				FROM #tblRelation
				WHERE Name = TABLE_NAME AND [Schema] = TABLE_SCHEMA) = 0
			)
			THEN(SELECT 0)
			
			WHEN(
			/*if tbl has fk, but all of them from his own columns, first generation tbl, 0*/
			(SELECT COUNT(*) 
				FROM #tblRelation
				WHERE Name = TABLE_NAME AND [Schema] = TABLE_SCHEMA) = 
			(SELECT COUNT(*) 
				FROM #tblRelation
				WHERE Name = TABLE_NAME
					AND FkFromTbl = TABLE_NAME  AND [Schema] = TABLE_SCHEMA)		
			)
			THEN(SELECT 0)
			
			/*tbl has fk, from other tbl columns, NULL*/
			ELSE(SELECT NULL)
		  END)	
		FROM INFORMATION_SCHEMA.TABLES 
			WHERE TABLE_TYPE = 'BASE TABLE'
			AND TABLE_NAME != 'sysdiagrams')
			
SELECT *
	FROM #tblDetail
	WHERE Generation IS NOT NULL
	ORDER BY Generation, Name, [Schema]




----------- Update genaretion's if not found yet =>

/*no first generation at relations*/
IF NOT EXISTS(SELECT * 
				FROM #tblDetail 
				WHERE Generation IS NOT NULL)
BEGIN
	PRINT 'No first generation table found at table relations.';
END


/*set new generation using olds*/
WHILE(EXISTS(SELECT * 
				FROM #tblDetail 
				WHERE Generation IS NULL))
BEGIN

	/*find new generation tables*/
	DECLARE @tblNewGeneration TABLE(Name VARCHAR(100) NOT NULL,
									[Schema] VARCHAR(100) NOT NULL)								
	/*Select tables where all of its foreign key table�s generation were found*/							
	INSERT INTO @tblNewGeneration
	SELECT Name, [Schema]
		FROM #tblDetail AS dtl
		WHERE Generation IS NULL
		AND NOT EXISTS (
				SELECT DISTINCT FkFromTbl,FkFromSchema
					FROM #tblRelation AS rel
					WHERE rel.Name = dtl.Name 
					AND rel.[Schema] = dtl.[Schema] 
				EXCEPT
				SELECT Name, [Schema]
					FROM #tblDetail
					WHERE Generation IS NOT NULL
		)

	/*no new generation table found from old genertaion tables*/
	IF(NOT EXISTS(SELECT * FROM @tblNewGeneration))
	BEGIN
		PRINT 'Circular flow found at table relations.';
		BREAK;
	END

	/*set generation*/
	DECLARE @crntGeneration INT
	SET @crntGeneration = (SELECT MAX(Generation)  
							FROM #tblDetail
							WHERE Generation IS NOT NULL);							
	UPDATE #tblDetail
		SET Generation = @crntGeneration + 1
		WHERE [Schema]+'.'+Name 
		IN (
			SELECT [Schema]+'.'+Name
				FROM @tblNewGeneration
		)
	
	/*clean the new generation tbls from temp*/	
	DELETE FROM @tblNewGeneration;
END


---- Result =>
SELECT *
	FROM #tblDetail
	ORDER BY Generation, Name, [Schema]
