USE [Todo]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 03/04/2015 19:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 03/04/2015 19:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[ContactNo] [nvarchar](20) NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[AddedDateTime] [datetime] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_dbo.User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Device]    Script Date: 03/04/2015 19:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Device](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](300) NULL,
	[Status] [int] NOT NULL,
	[AddedDateTime] [datetime] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_dbo.Device] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Event]    Script Date: 03/04/2015 19:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](300) NULL,
	[DeviceId] [bigint] NOT NULL,
	[Status] [int] NOT NULL,
	[AddedDateTime] [datetime] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_dbo.Event] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Trainee]    Script Date: 03/04/2015 19:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Trainee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[ContactNo] [nvarchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[EventId] [bigint] NOT NULL,
	[Status] [int] NOT NULL,
	[AddedDateTime] [datetime] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_dbo.Trainee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 03/04/2015 19:50:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Schedule](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[EventId] [bigint] NOT NULL,
	[StartingDateTime] [datetime] NULL,
	[EndingDateTime] [datetime] NULL,
	[Remarks] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[AddedDateTime] [datetime] NOT NULL,
	[AddedBy] [bigint] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_dbo.Schedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_dbo.Device_dbo.User_AddedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Device_dbo.User_AddedBy] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_dbo.Device_dbo.User_AddedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Device_dbo.User_UpdatedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Device]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Device_dbo.User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Device] CHECK CONSTRAINT [FK_dbo.Device_dbo.User_UpdatedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Event_dbo.Device_DeviceId]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Event_dbo.Device_DeviceId] FOREIGN KEY([DeviceId])
REFERENCES [dbo].[Device] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_dbo.Event_dbo.Device_DeviceId]
GO
/****** Object:  ForeignKey [FK_dbo.Event_dbo.User_AddedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Event_dbo.User_AddedBy] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_dbo.Event_dbo.User_AddedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Event_dbo.User_UpdatedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Event]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Event_dbo.User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Event] CHECK CONSTRAINT [FK_dbo.Event_dbo.User_UpdatedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Schedule_dbo.Event_EventId]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Schedule_dbo.Event_EventId] FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_dbo.Schedule_dbo.Event_EventId]
GO
/****** Object:  ForeignKey [FK_dbo.Schedule_dbo.User_AddedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Schedule_dbo.User_AddedBy] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_dbo.Schedule_dbo.User_AddedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Schedule_dbo.User_UpdatedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Schedule_dbo.User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_dbo.Schedule_dbo.User_UpdatedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Trainee_dbo.Event_EventId]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Trainee]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Trainee_dbo.Event_EventId] FOREIGN KEY([EventId])
REFERENCES [dbo].[Event] ([Id])
GO
ALTER TABLE [dbo].[Trainee] CHECK CONSTRAINT [FK_dbo.Trainee_dbo.Event_EventId]
GO
/****** Object:  ForeignKey [FK_dbo.Trainee_dbo.User_AddedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Trainee]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Trainee_dbo.User_AddedBy] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Trainee] CHECK CONSTRAINT [FK_dbo.Trainee_dbo.User_AddedBy]
GO
/****** Object:  ForeignKey [FK_dbo.Trainee_dbo.User_UpdatedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[Trainee]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Trainee_dbo.User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Trainee] CHECK CONSTRAINT [FK_dbo.Trainee_dbo.User_UpdatedBy]
GO
/****** Object:  ForeignKey [FK_dbo.User_dbo.User_AddedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_dbo.User_dbo.User_AddedBy] FOREIGN KEY([AddedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_dbo.User_dbo.User_AddedBy]
GO
/****** Object:  ForeignKey [FK_dbo.User_dbo.User_UpdatedBy]    Script Date: 03/04/2015 19:50:16 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_dbo.User_dbo.User_UpdatedBy] FOREIGN KEY([UpdatedBy])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_dbo.User_dbo.User_UpdatedBy]
GO
