/*http://www.codeproject.com/Tips/882314/String-Join-or-Split-in-SQL-Server*/


/*Join: List item to list string*/
--http://www.sql-server-helper.com/tips/tip-of-the-day.aspx?tkey=3934817c-1a03-4ac9-a0ba-55b2bfbaea0f&tkw=uses-of-the-stuff-string-function

DECLARE @tblId TABLE (Id BIGINT NOT NULL);
DECLARE @separatior VARCHAR(10);

INSERT INTO @tblId VALUES (1), (2), (3), (4);
SET @separatior = ',';

SELECT STUFF((SELECT @separatior + CONVERT(VARCHAR(100), Id)
			  FROM @tblId
			  ORDER BY Id
			  FOR XML PATH('')), 1, 1, '') AS Ids
			  


		
/*Split: List string to list*/
--www.sqlservercentral.com/blogs/querying-microsoft-sql-server/2013/09/19/how-to-split-a-string-by-delimited-char-in-sql-server/

DECLARE @nameList VARCHAR(MAX), @separatior VARCHAR(10);
SET @nameList = 'Dan, Han, Mos, Ben';
SET @separatior = ',';

DECLARE @tblName TABLE(Name VARCHAR(100));
DECLARE @fromIndex INT, @seperatiorAtIndex INT, @name VARCHAR(100);
SET @fromIndex = 1;
SET @seperatiorAtIndex = CHARINDEX(@separatior, @nameList) 

WHILE @fromIndex < LEN(@nameList) + 1 
BEGIN 
    IF @seperatiorAtIndex = 0  
        SET @seperatiorAtIndex = LEN(@nameList) + 1
	SET @name = LTRIM(RTRIM(SUBSTRING(@nameList, @fromIndex, @seperatiorAtIndex - @fromIndex))); 
	
    INSERT INTO @tblName VALUES(@name) -- inserting to the table
		
    SET @fromIndex = @seperatiorAtIndex + 1 
    SET @seperatiorAtIndex = CHARINDEX(@separatior, @nameList, @fromIndex)  
END 

SELECT *
	FROM @tblName
