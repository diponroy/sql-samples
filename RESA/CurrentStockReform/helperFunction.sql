USE [DbRESA_Live]
GO
/****** Object:  UserDefinedFunction [dbo].[GetInstalledNumber]    Script Date: 07/07/2013 13:38:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --DECLARE @strProduct VARCHAR(100), @strInstaller VARCHAR(100)
 --SET @strProduct = '00009';
 --SET @strInstaller = '00033';
 
 
 ALTER FUNCTION [dbo].[GetInstalledNumber](@strProduct VARCHAR(100), @strInstaller VARCHAR(100))
 RETURNS BIGINT
 AS 
 BEGIN
 
	 DECLARE @installedNumber BIGINT;
	 WITH Test AS  
	 (

 
		SELECT ProductInstalledByInstaller.ProdutId, ProductInstalledByInstaller.InstallerId, SUM(ProductInstalledByInstaller.Quantity) AS IntalledQuantity
			FROM
			(
				---- Dt4
				SELECT T02Dt4.Dt4ProductBrand1 AS ProdutId, Dt4InstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dt4
					WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand1)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
				UNION ALL
				SELECT T02Dt4.Dt4ProductBrand2 AS ProdutId, Dt4InstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dt4
					WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand2)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
					
				---- Dt5
				UNION ALL
				SELECT T02Dt5.Dt5ProductBrand1 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dt5
					WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand1)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
				UNION ALL
				SELECT T02Dt5.Dt5ProductBrand2 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dt5
					WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand2)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
				UNION ALL
				SELECT T02Dt5.Dt5ProductBrand3 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dt5
					WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand3)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
				UNION ALL
				SELECT T02Dt5.Dt5ProductBrand4 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dt5
					WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand4)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
					
											
				---- DtcSh
				UNION ALL
				SELECT T02Dtsh.DtshProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId, COALESCE(T02Dtsh.DtshQuantity1, 0) AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
					WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				UNION ALL
				SELECT T02Dtsh.DtshProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId, COALESCE(T02Dtsh.DtshQuantity2, 0) AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
					WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'

				---- DtWs
				UNION ALL		
				SELECT T02Dtws.DtwsProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId,  COALESCE(T02Dtws.DtwsQuntity1, 0) AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
					WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				UNION ALL		
				SELECT T02Dtws.DtwsProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId,  COALESCE(T02Dtws.DtwsQuntity2, 0) AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
					WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					
				---- DtSpc
				UNION ALL	
				SELECT T02Dtspc.DtspcProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
					WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
				UNION ALL	
				SELECT T02Dtspc.DtspcProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
					WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
				UNION ALL	
				SELECT T02Dtspc.DtspcProductBrand3 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
					WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				UNION ALL	
				SELECT T02Dtspc.DtspcProductBrand4 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
					WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					
				---- Lht
				UNION ALL	
				SELECT T02Dtlht.DtlhtProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity1 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
					WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				UNION ALL	
				SELECT T02Dtlht.DtlhtProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity2 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
					WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
				UNION ALL	
				SELECT T02Dtlht.DtlhtProductBrand3 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity3 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
					WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				UNION ALL	
				SELECT T02Dtlht.DtlhtProductBrand4 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity4 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
					WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
				UNION ALL	
				SELECT T02Dtlht.DtlhtProductBrand5 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity5 AS Quantity
					FROM T02Dtc
					LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
					WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand5)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
					
					
			) AS ProductInstalledByInstaller
			GROUP BY ProductInstalledByInstaller.ProdutId, ProductInstalledByInstaller.InstallerId
			HAVING LEN(LTRIM(RTRIM(ProductInstalledByInstaller.ProdutId))) > 0 
 

	 )	
	SELECT @installedNumber = IntalledQuantity
		FROM Test 
		WHERE ProdutId = @strProduct 
			AND InstallerId = @strInstaller
		
	IF(@installedNumber IS NULL)
	BEGIN
		SET @installedNumber = 0;
	END
				
	RETURN @installedNumber;
 END
