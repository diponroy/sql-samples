BEGIN TRY

	DECLARE @mainTran VARCHAR(100)
	SET @mainTran = 'mainTran';
	
	BEGIN TRANSACTION @mainTran
	
		------------------ Clear T02Cst =>
		--SELECT *
			--FROM T02Cst
		DELETE 
			FROM T02Cst
			
			
		--------- Clear Assign to installer >	
		DELETE 
			FROM T02Scd
			WHERE ScdOrderNo IN (SELECT SciOrderNo
									FROM T02Sci
									WHERE SciInstallerId = '')	
		DELETE 
			FROM T02Sci
			WHERE SciInstallerId = ''
			


		--------- Clear coustome return =>	
		DELETE
			FROM T02Srd	
			WHERE SrdReturnNo IN (SELECT ScrReturnNo
									FROM T02Scr
									WHERE ScrInstallerId = '')
		DELETE
			FROM T02Scr
			WHERE ScrInstallerId = ''


		------------------- Product Purchase =>
		--SELECT ProductParchaseDetail.ProductId, T01Pdt.PdtProductName AS ProductName, ProductParchaseDetail.ParchaseAmount, TotalProductAssigned.AssignedWithReturn AS TotalAssigned, (ProductParchaseDetail.ParchaseAmount-TotalProductAssigned.AssignedWithReturn) AS CurrentStock
		INSERT INTO T02Cst
		SELECT ProductParchaseDetail.ProductId, (ProductParchaseDetail.ParchaseAmount-TotalProductAssigned.AssignedWithReturn) AS CurrentStock, 0, 'ABC'
			FROM
			(
				SELECT T02Prd.PrdProductId AS ProductId, SUM(T02Prd.PrdQuantity) AS ParchaseAmount
					FROM T02Pur
					LEFT OUTER JOIN T02Prd ON T02Pur.PurOrderNo = T02Prd.PrdOrderNo	
					GROUP BY T02Prd.PrdProductId
			) AS ProductParchaseDetail
			
			LEFT OUTER JOIN T01Pdt ON ProductParchaseDetail.ProductId = T01Pdt.PdtProductId
			LEFT OUTER JOIN
			(
				SELECT ProductAssigned.ProductId AS ProductId,
						SUM(CASE WHEN(ProductReturn.ProductReturned IS NOT NULL) THEN (ProductAssigned.ProductAssigned - ProductReturn.ProductReturned)
								   WHEN(ProductReturn.ProductReturned IS NULL) THEN (ProductAssigned.ProductAssigned)
							  END) AS AssignedWithReturn
					FROM
					(
						SELECT T02Scd.ScdProductId AS ProductId, T02Sci.SciInstallerId AS InstallerId, SUM(T02Scd.ScdQuantity)AS ProductAssigned
							FROM T02Sci
							LEFT OUTER JOIN T02Scd ON T02Sci.SciOrderNo = T02Scd.ScdOrderNo
							GROUP BY T02Sci.SciInstallerId, T02Scd.ScdProductId
					) AS ProductAssigned
					LEFT OUTER JOIN
					(
						SELECT T02Srd.SrdProductId AS ProductId, T02Scr.ScrInstallerId AS InstallerId, SUM(T02Srd.SrdQuantity)AS ProductReturned
							FROM T02Scr
							LEFT OUTER JOIN T02Srd ON T02Scr.ScrReturnNo = T02Srd.SrdReturnNo
							GROUP BY T02Scr.ScrInstallerId, T02Srd.SrdProductId
					) AS ProductReturn
					ON ProductAssigned.ProductId = ProductReturn.ProductId
						AND ProductAssigned.InstallerId = ProductReturn.InstallerId
						
					WHERE LEN(ProductAssigned.InstallerId) <> 0
					GROUP BY ProductAssigned.ProductId
			) AS TotalProductAssigned
			ON ProductParchaseDetail.ProductId = TotalProductAssigned.ProductId;




		---------- Product Installed With Assigned	
		WITH InstallerAssignedProduct
		AS
		(	
			SELECT ProductAssigned.ProductId AS ProductId, ProductAssigned.InstallerId AS InstallerId, 
					CASE WHEN(ProductReturn.ProductReturned IS NOT NULL) THEN (ProductAssigned.ProductAssigned - ProductReturn.ProductReturned)
						 WHEN(ProductReturn.ProductReturned IS NULL) THEN (ProductAssigned.ProductAssigned)
					END AS AssignedWithReturn
				FROM
				(
					SELECT T02Scd.ScdProductId AS ProductId, T02Sci.SciInstallerId AS InstallerId, SUM(T02Scd.ScdQuantity)AS ProductAssigned
						FROM T02Sci
						LEFT OUTER JOIN T02Scd ON T02Sci.SciOrderNo = T02Scd.ScdOrderNo
						GROUP BY T02Sci.SciInstallerId, T02Scd.ScdProductId
				) AS ProductAssigned
				LEFT OUTER JOIN
				(
					SELECT T02Srd.SrdProductId AS ProductId, T02Scr.ScrInstallerId AS InstallerId, SUM(T02Srd.SrdQuantity)AS ProductReturned
						FROM T02Scr
						LEFT OUTER JOIN T02Srd ON T02Scr.ScrReturnNo = T02Srd.SrdReturnNo
						GROUP BY T02Scr.ScrInstallerId, T02Srd.SrdProductId
				) AS ProductReturn
				ON ProductAssigned.ProductId = ProductReturn.ProductId AND ProductAssigned.InstallerId = ProductReturn.InstallerId
		)
		--SELECT ProductId, T01Pdt.PdtProductName, InstallerId, (T01Itl.ItlFirstName +' ' +T01Itl.ItlLastName) AS InstallerName, AssignedWithReturn, dbo.GetInstalledNumber(ProductId, InstallerId) AS InstalledAmount, (AssignedWithReturn-dbo.GetInstalledNumber(ProductId, InstallerId)) AS CurrentStock
		INSERT INTO T02Cst
		SELECT ProductId, (AssignedWithReturn-dbo.GetInstalledNumber(ProductId, InstallerId)) AS CurrentStock, 0, InstallerId
			FROM InstallerAssignedProduct
			LEFT OUTER JOIN T01Pdt ON InstallerAssignedProduct.ProductId = T01Pdt.PdtProductId
			LEFT OUTER JOIN T01Itl ON InstallerAssignedProduct.InstallerId = T01Itl.ItlInstallerId
			WHERE LEN(InstallerId) <> 0
		
		
		---------- Product Installed Without Assigned
		--SELECT ProductInstalled.ProdutId, ProductInstalled.InstallerId, COALESCE(ProductdConsigned.ConsignedQuantity, 0) AS ConsignedQuantity, ProductInstalled.IntalledQuantity, (COALESCE(ProductdConsigned.ConsignedQuantity, 0)-ProductInstalled.IntalledQuantity) AS Stock
		INSERT INTO T02Cst
		SELECT ProductInstalled.ProdutId, (COALESCE(ProductdConsigned.ConsignedQuantity, 0)-ProductInstalled.IntalledQuantity) AS CurrentStock, 0, ProductInstalled.InstallerId
			FROM		
			(	
				
				SELECT ProductInstalledByInstaller.ProdutId, ProductInstalledByInstaller.InstallerId, SUM(ProductInstalledByInstaller.Quantity) AS IntalledQuantity
					FROM
					(
						---- Dt4
						SELECT T02Dt4.Dt4ProductBrand1 AS ProdutId, Dt4InstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dt4
							WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand1)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
						UNION ALL
						SELECT T02Dt4.Dt4ProductBrand2 AS ProdutId, Dt4InstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dt4
							WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand2)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
							
						---- Dt5
						UNION ALL
						SELECT T02Dt5.Dt5ProductBrand1 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dt5
							WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand1)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
						UNION ALL
						SELECT T02Dt5.Dt5ProductBrand2 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dt5
							WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand2)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
						UNION ALL
						SELECT T02Dt5.Dt5ProductBrand3 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dt5
							WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand3)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
						UNION ALL
						SELECT T02Dt5.Dt5ProductBrand4 AS ProdutId, Dt5InstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dt5
							WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand4)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
							
													
						---- DtcSh
						UNION ALL
						SELECT T02Dtsh.DtshProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId, COALESCE(T02Dtsh.DtshQuantity1, 0) AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
							WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						UNION ALL
						SELECT T02Dtsh.DtshProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId, COALESCE(T02Dtsh.DtshQuantity2, 0) AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
							WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'

						---- DtWs
						UNION ALL		
						SELECT T02Dtws.DtwsProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId,  COALESCE(T02Dtws.DtwsQuntity1, 0) AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
							WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						UNION ALL		
						SELECT T02Dtws.DtwsProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId,  COALESCE(T02Dtws.DtwsQuntity2, 0) AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
							WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'
							
						---- DtSpc
						UNION ALL	
						SELECT T02Dtspc.DtspcProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
							WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
						UNION ALL	
						SELECT T02Dtspc.DtspcProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
							WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
						UNION ALL	
						SELECT T02Dtspc.DtspcProductBrand3 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
							WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						UNION ALL	
						SELECT T02Dtspc.DtspcProductBrand4 AS ProdutId, DtcInstallerId AS InstallerId, 1 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
							WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'
							
						---- Lht
						UNION ALL	
						SELECT T02Dtlht.DtlhtProductBrand1 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity1 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
							WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						UNION ALL	
						SELECT T02Dtlht.DtlhtProductBrand2 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity2 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
							WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
						UNION ALL	
						SELECT T02Dtlht.DtlhtProductBrand3 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity3 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
							WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						UNION ALL	
						SELECT T02Dtlht.DtlhtProductBrand4 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity4 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
							WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
						UNION ALL	
						SELECT T02Dtlht.DtlhtProductBrand5 AS ProdutId, DtcInstallerId AS InstallerId, T02Dtlht.DtlhtQuntity5 AS Quantity
							FROM T02Dtc
							LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
							WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand5)) <> '' AND T02Dtc.DtcApproval = 'Approved'
										
					) AS ProductInstalledByInstaller
					GROUP BY ProductInstalledByInstaller.ProdutId, ProductInstalledByInstaller.InstallerId
					HAVING LEN(LTRIM(RTRIM(ProductInstalledByInstaller.ProdutId))) > 0

			) AS ProductInstalled

			LEFT OUTER JOIN
			(
					SELECT ProductAssigned.ProductId AS ProductId, ProductAssigned.InstallerId AS InstallerId, 
							CASE WHEN(ProductReturn.ProductReturned IS NOT NULL) THEN (ProductAssigned.ProductAssigned - ProductReturn.ProductReturned)
								 WHEN(ProductReturn.ProductReturned IS NULL) THEN (ProductAssigned.ProductAssigned)
							END AS ConsignedQuantity
						FROM
						(
							SELECT T02Scd.ScdProductId AS ProductId, T02Sci.SciInstallerId AS InstallerId, SUM(T02Scd.ScdQuantity)AS ProductAssigned
								FROM T02Sci
								LEFT OUTER JOIN T02Scd ON T02Sci.SciOrderNo = T02Scd.ScdOrderNo
								GROUP BY T02Sci.SciInstallerId, T02Scd.ScdProductId
						) AS ProductAssigned
						LEFT OUTER JOIN
						(
							SELECT T02Srd.SrdProductId AS ProductId, T02Scr.ScrInstallerId AS InstallerId, SUM(T02Srd.SrdQuantity)AS ProductReturned
								FROM T02Scr
								LEFT OUTER JOIN T02Srd ON T02Scr.ScrReturnNo = T02Srd.SrdReturnNo
								GROUP BY T02Scr.ScrInstallerId, T02Srd.SrdProductId
						) AS ProductReturn
						ON ProductAssigned.ProductId = ProductReturn.ProductId AND ProductAssigned.InstallerId = ProductReturn.InstallerId
			) AS ProductdConsigned
		ON ProductInstalled.ProdutId = ProductdConsigned.ProductId AND ProductInstalled.InstallerId = ProductdConsigned.InstallerId		
		WHERE ProductdConsigned.ConsignedQuantity IS NULL
		
	
	COMMIT TRANSACTION @mainTran
END TRY

BEGIN CATCH
	PRINT 'Error in Opretion';
	ROLLBACK TRANSACTION @mainTran
END CATCH


