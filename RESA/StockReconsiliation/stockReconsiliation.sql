WITH Reconciliation
AS
(
	SElECT 
			ROW_NUMBER() OVER(ORDER BY CONVERT(DATETIME, COALESCE(ProductConsigned.ActivityDate, ProductReturned.ActivityDate, ProductInstalled.ActivityDate, ProductRejected.ActivityDate), 103)) AS OrderId,
			COALESCE(ProductConsigned.ActivityDate, ProductReturned.ActivityDate, ProductInstalled.ActivityDate, ProductRejected.ActivityDate) AS ActivityDate,
			COALESCE(ProductConsigned.InstallerCompanyId, ProductReturned.InstallerCompanyId, ProductInstalled.InstallerCompanyId, ProductRejected.InstallerCompanyId) AS InstallerCompanyId,
			COALESCE(ProductConsigned.ProductId, ProductReturned.ProductId, ProductInstalled.ProductId, ProductRejected.ProductId) AS ProductId,
			
			COALESCE(ProductConsigned.ConsignmentNO, '') AS ConsignmentNO,
			
			COALESCE(ProductConsigned.ConsignedQuantity, 0) AS ConsignedQuantity, 
			COALESCE(ProductReturned.ReturnedQuantity, 0) AS ReturnedQuantity,
			COALESCE(ProductInstalled.IntalledQuantity, 0) AS IntalledQuantity,
			COALESCE(ProductRejected.RejectedQuantity, 0) AS RejectedQuantity

		FROM 
		(
			------------------ Product Consigned =>
			SELECT CONVERT(NVARCHAR(100), SciDate, 103) AS ActivityDate, T01Itl.ItlInstallerCompany AS InstallerCompanyId, T02Scd.ScdProductId AS ProductId, T02Sci.SciOrderNo AS ConsignmentNO,  SUM(T02Scd.ScdQuantity) AS ConsignedQuantity
				FROM T02Sci
				LEFT OUTER JOIN T02Scd ON T02Sci.SciOrderNo = T02Scd.ScdOrderNo
				INNER JOIN T01Itl ON T02Sci.SciInstallerId = T01Itl.ItlInstallerId
				GROUP BY CONVERT(NVARCHAR(100), SciDate, 103), T01Itl.ItlInstallerCompany, T02Scd.ScdProductId, T02Sci.SciOrderNo
					
		 ) AS ProductConsigned
		 
		 FULL OUTER JOIN
		 (
			------------------ Product Returned =>
			SELECT CONVERT(NVARCHAR(100), T02Scr.ScrDate, 103) AS ActivityDate, T01Itl.ItlInstallerCompany AS InstallerCompanyId, T02Srd.SrdProductId AS ProductId, SUM(T02Srd.SrdQuantity) AS ReturnedQuantity 
				FROM T02Scr
				LEFT OUTER JOIN T02Srd ON T02Scr.ScrReturnNo = T02Srd.SrdReturnNo
				INNER JOIN T01Itl ON T02Scr.ScrInstallerId = T01Itl.ItlInstallerId
				GROUP BY CONVERT(NVARCHAR(100), ScrDate, 103), T01Itl.ItlInstallerCompany, T02Srd.SrdProductId
				
		  ) AS ProductReturned ON ProductConsigned.ActivityDate = ProductReturned.ActivityDate 
									AND ProductConsigned.InstallerCompanyId = ProductReturned.InstallerCompanyId
									AND ProductConsigned.ProductId = ProductReturned.ProductId


		FULL OUTER JOIN 
		(
			------------------ Product Installed =>	
			SELECT ProductInstalledByInstaller.ActivityDate, ProductInstalledByInstaller.ProductId, ProductInstalledByInstaller.InstallerCompanyId, SUM(ProductInstalledByInstaller.Quantity) AS IntalledQuantity
				FROM
				(
					---- Dt4
					SELECT CONVERT(NVARCHAR(100), T02Dt4.Dt4InstallationDate, 103) AS ActivityDate, T02Dt4.Dt4ProductBrand1 AS ProductId, Dt4InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt4
						WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand1)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt4.Dt4InstallationDate, 103) AS ActivityDate, T02Dt4.Dt4ProductBrand2 AS ProductId, Dt4InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt4
						WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand2)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
						
					---- Dt5
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand1 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand1)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand2 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand2)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand3 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand3)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand4 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand4)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
												
					---- DtcSh
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtsh.DtshProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, COALESCE(T02Dtsh.DtshQuantity1, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
						WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtsh.DtshProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, COALESCE(T02Dtsh.DtshQuantity2, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
						WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'

					---- DtWs
					UNION ALL		
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtws.DtwsProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId,  COALESCE(T02Dtws.DtwsQuntity1, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
						WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					UNION ALL		
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtws.DtwsProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId,  COALESCE(T02Dtws.DtwsQuntity2, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
						WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						
					---- DtSpc
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand3 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand4 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'
						
					---- Lht
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity2 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand3 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity3 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand4 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity4 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand5 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity5 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand5)) <> '' AND T02Dtc.DtcApproval = 'Approved'			
									
						
				) AS ProductInstalledByInstaller
				GROUP BY ProductInstalledByInstaller.ActivityDate, ProductInstalledByInstaller.ProductId, ProductInstalledByInstaller.InstallerCompanyId
				HAVING LEN(LTRIM(RTRIM(ProductInstalledByInstaller.ProductId))) > 0
		 
		 ) AS ProductInstalled ON ProductConsigned.ActivityDate = ProductInstalled.ActivityDate 
									AND ProductConsigned.InstallerCompanyId = ProductInstalled.InstallerCompanyId
									AND ProductConsigned.ProductId = ProductInstalled.ProductId

		FULL OUTER JOIN 
		(
			------------------ Product Rejected =>	
			SELECT ProductRejectedOfInstaller.ActivityDate, ProductRejectedOfInstaller.ProductId, ProductRejectedOfInstaller.InstallerCompanyId, SUM(ProductRejectedOfInstaller.Quantity) AS RejectedQuantity
				FROM
				(
					---- Dt4
					SELECT CONVERT(NVARCHAR(100), T02Dt4.Dt4InstallationDate, 103) AS ActivityDate, T02Dt4.Dt4ProductBrand1 AS ProductId, Dt4InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt4
						WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand1)) <> '' AND T02Dt4.Dt4Approved = 'Reject'
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt4.Dt4InstallationDate, 103) AS ActivityDate, T02Dt4.Dt4ProductBrand2 AS ProductId, Dt4InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt4
						WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand2)) <> '' AND T02Dt4.Dt4Approved = 'Reject'
						
					---- Dt5
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand1 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand1)) <> '' AND T02Dt5.Dt5Approved = 'Reject'
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand2 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand2)) <> '' AND T02Dt5.Dt5Approved = 'Reject'	
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand3 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand3)) <> '' AND T02Dt5.Dt5Approved = 'Reject'	
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dt5.Dt5InstallationDate, 103) AS ActivityDate, T02Dt5.Dt5ProductBrand4 AS ProductId, Dt5InstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dt5
						WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand4)) <> '' AND T02Dt5.Dt5Approved = 'Reject'
												
					---- DtcSh
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtsh.DtshProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, COALESCE(T02Dtsh.DtshQuantity1, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
						WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Reject'
					UNION ALL
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtsh.DtshProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, COALESCE(T02Dtsh.DtshQuantity2, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
						WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Reject'

					---- DtWs
					UNION ALL		
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtws.DtwsProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId,  COALESCE(T02Dtws.DtwsQuntity1, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
						WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Reject'
					UNION ALL		
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtws.DtwsProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId,  COALESCE(T02Dtws.DtwsQuntity2, 0) AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
						WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Reject'
						
					---- DtSpc
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Reject'	
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Reject'	
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand3 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Reject'
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtspc.DtspcProductBrand4 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, 1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
						WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Reject'
						
					---- Lht
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand1 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity1 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Reject'
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand2 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity2 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Reject'			
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand3 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity3 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Reject'
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand4 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity4 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Reject'			
					UNION ALL	
					SELECT CONVERT(NVARCHAR(100), T02Dtc.DtcInstallationDate, 103) AS ActivityDate, T02Dtlht.DtlhtProductBrand5 AS ProductId, DtcInstallerCompanyName AS InstallerCompanyId, T02Dtlht.DtlhtQuntity5 AS Quantity
						FROM T02Dtc
						LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID	
						WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand5)) <> '' AND T02Dtc.DtcApproval = 'Reject'			
									
						
				) AS ProductRejectedOfInstaller
				GROUP BY ProductRejectedOfInstaller.ActivityDate, ProductRejectedOfInstaller.ProductId, ProductRejectedOfInstaller.InstallerCompanyId
				HAVING LEN(LTRIM(RTRIM(ProductRejectedOfInstaller.ProductId))) > 0	
				
		 ) AS ProductRejected ON ProductConsigned.ActivityDate = ProductRejected.ActivityDate 
									AND ProductConsigned.InstallerCompanyId = ProductRejected.InstallerCompanyId
									AND ProductConsigned.ProductId = ProductRejected.ProductId
		
		
		----------- InstallerCompanyId And ProductId Check => 							
		WHERE COALESCE(ProductConsigned.InstallerCompanyId, ProductReturned.InstallerCompanyId, ProductInstalled.InstallerCompanyId, ProductRejected.InstallerCompanyId) = '00032'
			  AND COALESCE(ProductConsigned.ProductId, ProductReturned.ProductId, ProductInstalled.ProductId, ProductRejected.ProductId) = '00009'
 ) 
 
SELECT StockReconciliation.*, (SELECT SUM(aReconciliation.ConsignedQuantity) -SUM(aReconciliation.ReturnedQuantity) -SUM(aReconciliation.IntalledQuantity) +SUM(aReconciliation.RejectedQuantity)
									FROM Reconciliation AS aReconciliation
									WHERE aReconciliation.OrderId <= StockReconciliation.OrderId) AS Balance, (CASE 
																													WHEN( StockReconciliation.ConsignmentNO = '') THEN(SELECT '')
																													WHEN( SUBSTRING(StockReconciliation.ConsignmentNO, 1, 2) = 'SC') THEN(SELECT '../../consignment/' +StockReconciliation.ConsignmentNO)
																													WHEN( SUBSTRING(StockReconciliation.ConsignmentNO, 1, 2) = 'SR') THEN(SELECT '../../consignmentreturn/' +StockReconciliation.ConsignmentNO)
																													END) AS FileLocation
	FROM Reconciliation AS StockReconciliation

