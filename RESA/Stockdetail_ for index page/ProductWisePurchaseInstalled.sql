-------- Product
SELECT Product.PdtProductId, Product.PdtProductName 
	FROM T01Pdt AS Product

------ 	ProductPurchase
SELECT T02Prd.PrdProductId AS ProductId, SUM(T02Prd.PrdQuantity) AS PurchasedQuantity
	FROM T02Pur
	LEFT OUTER JOIN T02Prd ON T02Pur.PurOrderNo = T02Prd.PrdOrderNo
	GROUP BY T02Prd.PrdProductId
	
------ ProductAssigned
SElECT T02Scd.ScdProductId AS ProductId, SUM(T02Scd.ScdQuantity) AS AssignedQuantity
	FROM T02Sci
	LEFT OUTER JOIN T02Scd ON T02Sci.SciOrderNo = T02Scd.ScdOrderNo
	GROUP BY T02Scd.ScdProductId

----- ProductReturned
SELECT T02Srd.SrdProductId AS ProductId, SUM(T02Srd.SrdQuantity) AS ReturnedQuantity
	FROM T02Scr 
	LEFT OUTER JOIN T02Srd ON T02Scr.ScrReturnNo = T02Srd.SrdReturnNo
	GROUP BY T02Srd.SrdProductId
	
---- ProductInstalled

	


---- Main Query
SELECT Product.PdtProductId AS ProductId, Product.PdtProductName AS ProductName,
	   COALESCE(ProductPurchase.PurchasedQuantity, 0) AS PurchasedQuantity,
	   COALESCE(ProductAssigned.AssignedQuantity, 0) AS AssignedQuantity,
	   COALESCE(ProductReturned.ReturnedQuantity, 0) AS ReturnedQuantity,
	   (COALESCE(ProductAssigned.AssignedQuantity, 0) - COALESCE(ProductReturned.ReturnedQuantity, 0)) AS ConsignedQuantity,
	   COALESCE(ProductInstalled.ProductInstalled, 0) AS InstalledQuantity,
	   --ConsignedQuantity - InstalledQuantity AS RemainedQuantity
	   (COALESCE(ProductAssigned.AssignedQuantity, 0) - COALESCE(ProductReturned.ReturnedQuantity, 0)) - COALESCE(ProductInstalled.ProductInstalled, 0) AS RemainedQuantity
	   
	FROM T01Pdt AS Product

	------ 	ProductPurchase
	LEFT OUTER JOIN	
	(SELECT T02Prd.PrdProductId AS ProductId, SUM(T02Prd.PrdQuantity) AS PurchasedQuantity
		FROM T02Pur
		LEFT OUTER JOIN T02Prd ON T02Pur.PurOrderNo = T02Prd.PrdOrderNo
		GROUP BY T02Prd.PrdProductId) AS ProductPurchase
	ON Product.PdtProductId = ProductPurchase.ProductId
	
	----- ProductAssigned
	LEFT OUTER JOIN
	(SElECT T02Scd.ScdProductId AS ProductId, SUM(T02Scd.ScdQuantity) AS AssignedQuantity
		FROM T02Sci
		LEFT OUTER JOIN T02Scd ON T02Sci.SciOrderNo = T02Scd.ScdOrderNo
		GROUP BY T02Scd.ScdProductId) AS ProductAssigned
	ON Product.PdtProductId = ProductAssigned.ProductId
	
	
	----- ProductReturned
	LEFT OUTER JOIN
	(SELECT T02Srd.SrdProductId AS ProductId, SUM(T02Srd.SrdQuantity) AS ReturnedQuantity
		FROM T02Scr 
		LEFT OUTER JOIN T02Srd ON T02Scr.ScrReturnNo = T02Srd.SrdReturnNo
		GROUP BY T02Srd.SrdProductId) AS ProductReturned
	ON Product.PdtProductId = ProductReturned.ProductId
	
	
	----- ProductInstalled
	LEFT OUTER JOIN
	(
		
		SELECT InstalledProductList.ProdutId AS ProductId, SUM(Quantity) AS ProductInstalled
		FROM
		(
			---- Dt4
			SELECT T02Dt4.Dt4ProductBrand1 AS ProdutId, 1 AS Quantity
				FROM T02Dt4
				WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand1)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
			UNION ALL
			SELECT T02Dt4.Dt4ProductBrand2 AS ProdutId, 1 AS Quantity
				FROM T02Dt4
				WHERE LTRIM(RTRIM(T02Dt4.Dt4ProductBrand2)) <> '' AND T02Dt4.Dt4Approved = 'Approved'
				
			---- Dt5
			UNION ALL
			SELECT T02Dt5.Dt5ProductBrand1 AS ProdutId, 1 AS Quantity
				FROM T02Dt5
				WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand1)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
			UNION ALL
			SELECT T02Dt5.Dt5ProductBrand2 AS ProdutId, 1 AS Quantity
				FROM T02Dt5
				WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand2)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
			UNION ALL
			SELECT T02Dt5.Dt5ProductBrand3 AS ProdutId, 1 AS Quantity
				FROM T02Dt5
				WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand3)) <> '' AND T02Dt5.Dt5Approved = 'Approved'	
			UNION ALL
			SELECT T02Dt5.Dt5ProductBrand4 AS ProdutId, 1 AS Quantity
				FROM T02Dt5
				WHERE LTRIM(RTRIM(T02Dt5.Dt5ProductBrand4)) <> '' AND T02Dt5.Dt5Approved = 'Approved'
				
										
			---- DtcSh
			UNION ALL
			SELECT T02Dtsh.DtshProductBrand1 AS ProdutId, COALESCE(T02Dtsh.DtshQuantity1, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
				WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL
			SELECT T02Dtsh.DtshProductBrand2 AS ProdutId, COALESCE(T02Dtsh.DtshQuantity2, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtsh ON T02Dtc.DtcID = T02Dtsh.DtshCustomerID
				WHERE LTRIM(RTRIM(T02Dtsh.DtshProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			
			
			---- DtWs
			UNION ALL		
			SELECT T02Dtws.DtwsProductBrand1 AS ProdutId,  COALESCE(T02Dtws.DtwsQuntity1, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
				WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL		
			SELECT T02Dtws.DtwsProductBrand2 AS ProdutId,  COALESCE(T02Dtws.DtwsQuntity2, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtws ON T02Dtc.DtcID = T02Dtws.DtwsCustomerID
				WHERE LTRIM(RTRIM(T02Dtws.DtwsProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				
			---- DtSpc
			UNION ALL	
			SELECT T02Dtspc.DtspcProductBrand1, 1 AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
				WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
			UNION ALL	
			SELECT T02Dtspc.DtspcProductBrand2, 1 AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
				WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'	
			UNION ALL	
			SELECT T02Dtspc.DtspcProductBrand3, 1 AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
				WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL	
			SELECT T02Dtspc.DtspcProductBrand4, 1 AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtspc ON T02Dtc.DtcID = T02Dtspc.DtspcCustomerID	
				WHERE LTRIM(RTRIM(T02Dtspc.DtspcProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'
				
			--------Lht	
			UNION ALL
			SELECT T02Dtlht.DtlhtProductBrand1 AS ProdutId, COALESCE(T02Dtlht.DtlhtQuntity1, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID
				WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand1)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL
			SELECT T02Dtlht.DtlhtProductBrand2 AS ProdutId, COALESCE(T02Dtlht.DtlhtQuntity2, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID
				WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand2)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL	
			SELECT T02Dtlht.DtlhtProductBrand3 AS ProdutId, COALESCE(T02Dtlht.DtlhtQuntity3, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID
				WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand3)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL	
			SELECT T02Dtlht.DtlhtProductBrand4 AS ProdutId, COALESCE(T02Dtlht.DtlhtQuntity4, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID
				WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand4)) <> '' AND T02Dtc.DtcApproval = 'Approved'
			UNION ALL	
			SELECT T02Dtlht.DtlhtProductBrand5 AS ProdutId, COALESCE(T02Dtlht.DtlhtQuntity5, 0) AS Quantity
				FROM T02Dtc
				LEFT OUTER JOIN T02Dtlht ON T02Dtc.DtcID = T02Dtlht.DtlhtCustomerID
				WHERE LTRIM(RTRIM(T02Dtlht.DtlhtProductBrand5)) <> '' AND T02Dtc.DtcApproval = 'Approved'
		) AS InstalledProductList
		GROUP BY ProdutId	
					
	) AS ProductInstalled
	ON Product.PdtProductId = ProductInstalled.ProductId
	ORDER BY Product.PdtProductName