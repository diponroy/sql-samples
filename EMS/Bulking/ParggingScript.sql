DECLARE @dbMain VARCHAR(100), @dbHistory VARCHAR(100)
DECLARE @fixedTblList VARCHAR(MAX)

SET @dbMain = 'ExamSystemTest1';
SET @dbHistory = 'DbHistory';
SET @fixedTblList = 'applicationstatus, apputility, auditaction, gradetype, permission, userrole, usertype';




--------------- populate the fixed table name =>
DECLARE @tblFixed TABLE(Name VARCHAR(100) NOT NULL)
DECLARE @aTblName VARCHAR(100)
DECLARE @index INT
SET @index = 1;

WHILE (CHARINDEX(',', @fixedTblList) > 0)
BEGIN
	SET @index = CHARINDEX(',', @fixedTblList);
	SET @aTblName = LTRIM(RTRIM(SUBSTRING(@fixedTblList, 1, @index-1))); 
	INSERT INTO @tblFixed(Name) VALUES(@aTblName)
	SET @fixedTblList = LTRIM(RTRIM(SUBSTRING(@fixedTblList, @index+1, LEN(@fixedTblList))));
	SET @index = CHARINDEX(',', @fixedTblList);
END
IF (LEN(@fixedTblList) > 0)
BEGIN
	SET @aTblName = LTRIM(RTRIM(SUBSTRING(@fixedTblList, @index+1, LEN(@fixedTblList))));
	INSERT INTO @tblFixed(Name) VALUES(@aTblName) 
	SET @fixedTblList = '';
END
--------------- populate the fixed table name <=





--------------- Get child table =>
DECLARE @tblChild TABLE(ParentTbl VARCHAR(100) NOT NULL,
						ParentClm VARCHAR(100) NOT NULL,
						ChildTbl VARCHAR(100) NOT NULL,
						ChildClm VARCHAR(100) NOT NULL)
INSERT
	INTO @tblChild
	SELECT
		OBJECT_NAME(parent_object_id) 'Parent table',
		c.NAME 'Parent column name',
		OBJECT_NAME(referenced_object_id) 'Referenced table',
		cref.NAME 'Referenced column name'
	FROM 
		sys.foreign_key_columns fkc
	INNER JOIN 
		sys.columns c 
		   ON fkc.parent_column_id = c.column_id 
			  AND fkc.parent_object_id = c.object_id
	INNER JOIN 
		sys.columns cref 
		   ON fkc.referenced_column_id = cref.column_id 
			  AND fkc.referenced_object_id = cref.object_id
--------- Get Child Table <=






---------- Get Tbl generations and detail =>
DECLARE @tblDetail TABLE(Name VARCHAR(100) NOT NULL,
						 Generation INT NULL,
						 IdentityColumn VARCHAR(100) NOT NULL,
						 IsFixed BIT NULL)
INSERT 
	INTO @tblDetail(Name, IdentityColumn, Generation, IsFixed)
	(SELECT OBJECT_NAME(object_id), CONVERT(VARCHAR(100), name),
		(CASE WHEN((SELECT COUNT(*) FROM sys.foreign_key_columns WHERE OBJECT_NAME(parent_object_id) = OBJECT_NAME(object_id)) > 0)
			    THEN(SELECT NULL)
			  ELSE(SELECT 0)
		  END), 0
		 -- (CASE WHEN(OBJECT_NAME(object_id) IN(SELECT * FROM @tblFixed))
			--		THEN(1)
			--	ELSE(0)	
			--END)		
		FROM sys.identity_columns AS tableClms
			WHERE OBJECT_NAME(object_id) 
				IN(SELECT TABLE_NAME 
						FROM INFORMATION_SCHEMA.TABLES 
						WHERE TABLE_TYPE = 'BASE TABLE'
						AND TABLE_NAME != 'sysdiagrams'))

--------- if table exists in fixed isfixed = 1
UPDATE @tblDetail
	SET IsFixed = 1
	WHERE Name IN(SELECT * FROM @tblFixed)

----------- set the genaretions
DECLARE @crntGeneration INT
DECLARE @crntTbl VARCHAR(100)

WHILE((SELECT COUNT(*) FROM @tblDetail WHERE Generation IS NULL) > 0)
BEGIN
	SET @crntGeneration = (SELECT MAX(Generation)  
							FROM @tblDetail
							WHERE Generation IS NOT NULL);

	DECLARE childTblCursor CURSOR FOR
		SELECT ParentTbl
			FROM @tblChild
			WHERE ChildTbl IN(SELECT Name
								FROM @tblDetail
								WHERE Generation <= @crntGeneration)
		EXCEPT
		SELECT ParentTbl
			FROM @tblChild
			WHERE CHildTbl IN(SELECT Name
									FROM @tblDetail
									WHERE Generation > @crntGeneration OR Generation IS NULL)
	OPEN childTblCursor
		FETCH NEXT FROM childTblCursor INTO @crntTbl
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE @tblDetail
				SET Generation = @crntGeneration + 1
				WHERE Name = @crntTbl
					AND Generation IS NULL
			FETCH NEXT FROM childTblCursor INTO @crntTbl
		END
	CLOSE childTblCursor
	DEALLOCATE childTblCursor
END
---------- Get Tbl generations and detail <=
	


--SELECT *
--	FROM @tblDetail
--	ORDER BY Generation DESC, Name

-------- All opretion under the mainTran
DECLARE @mainTran VARCHAR(100)
SET @mainTran = 'mainTran';


BEGIN TRY
	DECLARE @crntColumnName VARCHAR(100)
	DEClARE @setString VARCHAR(MAX)
	DECLARE @lastIndex INT

	BEGIN TRANSACTION @mainTran
		DECLARE @primaryKey VARCHAR(100)
		DECLARE @isFixed BIT
		DECLARE @SQL VARCHAR(MAX)


		--------- Insert Into history Db and update
		DECLARE tblCoursor CURSOR FOR
			SELECT *
				FROM @tblDetail
				ORDER BY Generation ASC, Name

		OPEN tblCoursor
			FETCH NEXT FROM tblCoursor INTO @crntTbl, @crntGeneration, @primaryKey, @isFixed
			WHILE(@@FETCH_STATUS = 0)
			BEGIN
				----------- Insert in histroy if doesn't exist
				SET @SQL =
							'INSERT ' 
								+ 'INTO ' + @dbHistory +'.[dbo].[' +@crntTbl +'] '
								+ 'SELECT * '
								+ 'FROM ' +@dbMain +'.[dbo].[' +@crntTbl +'] '
								+ 'WHERE ' +@primaryKey +' NOT IN(SELECT ' +@primaryKey +' '
																		+ 'FROM ' +@dbHistory +'.[dbo].[' +@crntTbl +']) ';
				--PRINT(@SQL)
				EXEC(@SQL)



				------------ Update histroy if exists
				SET @setString = ''
				DECLARE clmCursor CURSOR FOR
					SELECT COLUMN_NAME
						FROM INFORMATION_SCHEMA.COLUMNS
						WHERE TABLE_NAME = @crntTbl

				OPEN clmCursor
					FETCH NEXT FROM clmCursor INTO @crntColumnName
					WHILE @@FETCH_STATUS = 0
					BEGIN
						SET @setString = @setString + @dbHistory +'.[dbo].[' + @crntTbl +'].['+@crntColumnName+'] = ' +'[aMainTbl].['+@crntColumnName+'], '
						FETCH NEXT FROM clmCursor INTO @crntColumnName
					END
				CLOSE clmCursor
				DEALLOCATE clmCursor
				SET @setString =  RTRIM(LTRIM(@setString));
				SET @lastIndex = LEN(@setString) - CHARINDEX(',', REVERSE(@setString));
				SET @setString = SUBSTRING(@setString, 1, @lastIndex)
				--PRINT(@setString)

				SET @SQL =
							'UPDATE ' +@dbHistory +'.[dbo].[' +@crntTbl +']'
								+ ' SET ' + @setString 
								+ ' FROM ' +@dbMain +'.[dbo].[' +@crntTbl +'] AS aMainTbl'
								+ ' WHERE [aMainTbl].[' +@primaryKey +'] = ' +@dbHistory +'.[dbo].[' +@crntTbl +'].[' +@primaryKey +']';
				--PRINT(@SQL)
				EXEC(@SQL)


			FETCH NEXT FROM tblCoursor INTO @crntTbl, @crntGeneration, @primaryKey, @isFixed
			END
		CLOSE tblCoursor
		DEALLOCATE tblCoursor


		-------- Delete from maintable if it is not fixed
		DECLARE tblCoursor CURSOR FOR
			SELECT *
				FROM @tblDetail
				WHERE IsFixed = 0
				ORDER BY Generation DESC, Name	
				 
		OPEN tblCoursor
			FETCH NEXT FROM tblCoursor INTO @crntTbl, @crntGeneration, @primaryKey, @isFixed
			WHILE(@@FETCH_STATUS = 0)
			BEGIN

				IF(@crntTbl = 'user')
					BEGIN
						SET @SQL =
									  'DElETE' 
										+ ' FROM ' + @dbMain +'.[dbo].[' +@crntTbl +']'
										+ ' WHERE ' + @dbMain +'.[dbo].[' +@crntTbl +'].['+@primaryKey+']'
											+ ' NOT IN(SELECT '+ @primaryKey +
														+ ' FROM ' + @dbMain +'.[dbo].[' +@crntTbl +']'
														+ ' WHERE ' + @dbMain +'.[dbo].[' +@crntTbl +'].[userTypeID] = 1'
															+ ' AND ' + @dbMain +'.[dbo].[' +@crntTbl +'].[userIsDeleted] = ''no'')'

					END
				ELSE
					BEGIN
						SET @SQL =
									'DElETE ' 
										+ 'FROM ' + @dbMain +'.[dbo].[' +@crntTbl +'] '
					END
				--PRINT(@SQL)
				EXEC(@SQL)
			FETCH NEXT FROM tblCoursor INTO @crntTbl, @crntGeneration, @primaryKey, @isFixed
			END
		CLOSE tblCoursor
		DEALLOCATE tblCoursor


	COMMIT TRANSACTION @mainTran
END TRY
	
BEGIN CATCH
	PRINT 'Unexpected error occurred!' 
	ROLLBACK TRANSACTION @mainTran
END CATCH

