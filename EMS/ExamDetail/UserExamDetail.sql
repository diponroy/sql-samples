---------------- Select Unick Exams => 
SELECT examschedule.examScheduleCode AS ScheduleCode,
		CONVERT(VARCHAR(10), examschedule.examScheduleStartDateTime, 103) + ' ' + RIGHT(CONVERT(VARCHAR, examschedule.examScheduleStartTime, 100), 7) AS ExamStarted, 
		CONVERT(VARCHAR(10), examschedule.examScheduleEndDateTime, 103) + ' ' + RIGHT(CONVERT(VARCHAR, examschedule.examScheduleEndTime, 100), 7) AS ExamEnded, 
	   exam.examCode AS ExamCode, unit.unitName AS UnitName, unit.unitCode AS UnitCode, COUNT(userexam.examineeID) AS ExamineeCount
	FROM userexam
	INNER JOIN examschedule ON userexam.examScheduleID = examschedule.examID
	INNER JOIN exam ON examschedule.examID = exam.examID
	INNER JOIN unit ON exam.unitID = unit.unitID
	WHERE userexam.examStarted BETWEEN '06-01-2013' AND GETDATE()
	GROUP BY examschedule.examScheduleCode, (CONVERT(VARCHAR(10), examschedule.examScheduleStartDateTime, 103) + ' ' + RIGHT(CONVERT(VARCHAR, examschedule.examScheduleStartTime, 100), 7)), (CONVERT(VARCHAR(10), examschedule.examScheduleEndDateTime, 103) + ' ' + RIGHT(CONVERT(VARCHAR, examschedule.examScheduleEndTime, 100), 7)), exam.examCode, unit.unitName, unit.unitCode
	HAVING COUNT(userexam.examineeID) > 2;
---------------- Select Unick Exams <=




------------------- User Wise Exam Detail =>
WITH ExamDetail
AS
(
	SELECT 
		   examschedule.examScheduleID AS ScheduleId, examschedule.examScheduleCode AS ScheduleCode,
		   exam.examID AS ExamId, exam.examCode AS ExamCode, exam.isQuestionTimeConsiderable AS IsQuestionTimeConsiderable, 
		   unit.unitID AS UnitId, unit.unitName AS UnitName, unit.unitCode AS UnitCode,
		   
		   ----------- Time Duretions =>
		   examschedule.examScheduleGracePeriod AS [ScheduleGracePeriod(mm)],
		   exam.examDuration AS [ExamDuration(mm)],
		   exam.examGracePeriod AS [ExamGracePeriod(mm)],
		   ExamQuestionLevel.[QuestionLevelDuration(ss)],
		   
		   -------- Total Time
		   CONVERT
		   (VARCHAR,
			   DATEADD
			   (S,
				   CASE WHEN (exam.isQuestionTimeConsiderable = 'no')   THEN( (examschedule.examScheduleGracePeriod+exam.examDuration+exam.examGracePeriod)*60 )
						WHEN (exam.isQuestionTimeConsiderable = 'yes')  THEN( ExamQuestionLevel.[QuestionLevelDuration(ss)] )
						END,
				'19700101'
			   ),
			108
		   )AS ExamTotalTime
		   
		FROM examschedule
		INNER JOIN exam ON examschedule.examID = exam.examID
		INNER JOIN unit ON exam.unitID = unit.unitID
		INNER JOIN
		------- Question Level Time for an exam 
		(
			SELECT questionsexam.examID AS ExamId, SUM(question.questionDurationSec) AS [QuestionLevelDuration(ss)]
				FROM questionsexam
				INNER JOIN question ON questionsexam.questionID = question.questionID
				WHERE question.isActive = 'yes'
					AND question.isDeleted = 'no'
				GROUP BY questionsexam.examID
				
		) AS ExamQuestionLevel ON exam.examID = ExamQuestionLevel.ExamId
)
SELECT userexam.*, 
	
	ExamDetail.ScheduleCode,
	ExamDetail.ExamCode,
	ExamDetail.UnitName,
	ExamDetail.ExamTotalTime, 
	CONVERT(VARCHAR, userexam.examFinished-userexam.examStarted, 108) AS UserConsignedTime,
	CONVERT(VARCHAR(10), userexam.examStarted, 103) + ' ' + RIGHT(CONVERT(VARCHAR, userexam.examStarted, 100), 7) AS ExamStarted,
	CONVERT(VARCHAR(10), userexam.examFinished, 103) + ' ' + RIGHT(CONVERT(VARCHAR, userexam.examFinished, 100), 7) AS ExamFinished
	FROM userexam
	LEFT OUTER JOIN ExamDetail ON userexam.examScheduleID = ExamDetail.ScheduleId
	--WHERE userexam.examSubmitStatus <> 'User Submited'
	--WHERE userexam.examSubmitStatus = 'Auto Submited'
	--WHERE userexam.examSubmitStatus = 'Colse Window Submit'
	WHERE userexam.examSubmitStatus = 'Running'
------------------- User Wise Exam Detail <=	



SELECT *
	FROM [user]
	WHERE [user].userID IN (SELECT userexam.userID
								FROM userexam
								WHERE userexam.examSubmitStatus <> 'User Submited')
								
								
--SELECT *
--	FROM userexam
--		WHERE userID = (SELECT userID
--							FROM [user] WHERE [user].userLogin = 'Dipon')