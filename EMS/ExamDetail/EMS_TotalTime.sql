SELECT 
	   examschedule.examScheduleID AS ScheduleId, examschedule.examScheduleCode AS ScheduleCode,
	   exam.examID AS ExamId, exam.examCode AS ExamCode, exam.isQuestionTimeConsiderable AS IsQuestionTimeConsiderable, 
	   unit.unitID AS UnitId, unit.unitName AS UnitName, unit.unitCode AS UnitCode,
	   
	   ----------- Time Duretions =>
	   examschedule.examScheduleGracePeriod AS [ScheduleGracePeriod(mm)],
	   exam.examDuration AS [ExamDuration(mm)],
	   exam.examGracePeriod AS [ExamGracePeriod(mm)],
	   ExamQuestionLevel.[QuestionLevelDuration(ss)],
	   
	   -------- Total Time
	   CONVERT
	   (VARCHAR,
		   DATEADD
		   (S,
			   CASE WHEN (exam.isQuestionTimeConsiderable = 'no')   THEN( (examschedule.examScheduleGracePeriod+exam.examDuration+exam.examGracePeriod)*60 )
					WHEN (exam.isQuestionTimeConsiderable = 'yes')  THEN( ExamQuestionLevel.[QuestionLevelDuration(ss)] )
					END,
			'19700101'
		   ),
	    108
	   )AS ExamTotalTime
	   
	FROM examschedule
	INNER JOIN exam ON examschedule.examID = exam.examID
	INNER JOIN unit ON exam.unitID = unit.unitID
	INNER JOIN
	------- Question Level Time for an exam 
	(
		SELECT questionsexam.examID AS ExamId, SUM(question.questionDurationSec) AS [QuestionLevelDuration(ss)]
			FROM questionsexam
			INNER JOIN question ON questionsexam.questionID = question.questionID
			WHERE question.isActive = 'yes'
				AND question.isDeleted = 'no'
			GROUP BY questionsexam.examID
			
	) AS ExamQuestionLevel ON exam.examID = ExamQuestionLevel.ExamId
