SELECT aUnit.unitID AS UnitId, aUnit.unitName AS UnitName, aUnit.unitCode AS UnitCode,
	   aSchedule.examScheduleID AS ScheduleId, aSchedule.examScheduleCode AS ScheduleCode,
	   aExam.examID AS ExamId, aExam.examTitle AS ExamTitle, aExam.examCode AS ExamCode,
	   
	   COALESCE(UserAttached.AttachedASUser, 0) AS AttachedASUser,
	   COALESCE(GroupAttached.AttachedFromGroup, 0) AS AttachedFromGroup,
	   COALESCE(UserAttached.AttachedASUser, 0) + COALESCE(GroupAttached.AttachedFromGroup, 0) AS TotalAttached,
	   
	   COALESCE(AttentedExaminee.TotalAttentedExaminee, 0) AS AttantedExaminee,
	   COALESCE(AttentedTester.TotalAttentedTest, 0) AS AttantedTester,
	   COALESCE(AttentedExaminee.TotalAttentedExaminee, 0) + COALESCE(AttentedTester.TotalAttentedTest, 0) AS  TotalAttented,
	   
	   (COALESCE(UserAttached.AttachedASUser, 0) + COALESCE(GroupAttached.AttachedFromGroup, 0)) - (COALESCE(AttentedExaminee.TotalAttentedExaminee, 0) + COALESCE(AttentedTester.TotalAttentedTest, 0)) AS TotalRemainToAttent,
	   
	   COALESCE(ExamSubmitDetail.[Auto Submited], 0) AS [Auto Submited],
	   COALESCE(ExamSubmitDetail.[User Submited], 0) AS [User Submited],
	   COALESCE(ExamSubmitDetail.[Colse Window Submit], 0) AS [Colse Window Submit],
	   COALESCE(ExamSubmitDetail.Running, 0) AS Running,
	   COALESCE(ExamSubmitDetail.Disconnected, 0) AS Disconnected
	   
	FROM
	(
		SELECT *
			FROM unit 	
	 ) AS aUnit

	 LEFT OUTER JOIN 
	(
		SELECT *
			FROM examschedule
	) AS aSchedule ON aUnit.unitID = aSchedule.unitId
	
	LEFT OUTER JOIN 
	(
		SELECT *
			FROM exam	
	 ) AS aExam ON aSchedule.examID = aExam.examID
	
	LEFT OUTER JOIN 
	(
		SELECT examScheduleID AS SheduleId, COUNT(examSceduleUserID) AS AttachedASUser
			FROM examschedule_user
				LEFT OUTER JOIN [user] ON examschedule_user.userID = [user].userID
			WHERE [user].userIsActive = 'yes' 
				AND [user].userIsDeleted = 'no'
			GROUP BY examScheduleID	
	 ) AS UserAttached ON aSchedule.examScheduleID = UserAttached.SheduleId
	
	LEFT OUTER JOIN
	(
		SElECT examschedule_usergroup.examScheduleID AS SheduleId, COUNT(user_usergroup.userID) AS AttachedFromGroup
			FROM examschedule_usergroup
				LEFT OUTER JOIN user_usergroup ON examschedule_usergroup.userGroupID = user_usergroup.userGroupID
				LEFT OUTER JOIN [user] ON user_usergroup.userID = [user].userID
			WHERE [user].userIsActive = 'yes' 
				AND [user].userIsDeleted = 'no'	
			GROUP BY examschedule_usergroup.examScheduleID	
	 ) AS GroupAttached ON aSchedule.examScheduleID = GroupAttached.SheduleId

	LEFT OUTER JOIN 
	(
		SELECT examScheduleID AS ScheduleId, COUNT(examineeID) AS TotalAttentedExaminee
			FROM userexam
				LEFT OUTER JOIN [user] ON userexam.userID = [user].userID
			WHERE [user].userTypeID = 4 
			GROUP BY examScheduleID	
	 ) AS AttentedExaminee ON aSchedule.examScheduleID = AttentedExaminee.ScheduleId

	LEFT OUTER JOIN 
	(
		SELECT examScheduleID AS ScheduleId, COUNT(examineeID) AS TotalAttentedTest
			FROM userexam
				LEFT OUTER JOIN [user] ON userexam.userID = [user].userID
			WHERE [user].userTypeID <> 4 
			GROUP BY examScheduleID	
	 )AS AttentedTester ON aSchedule.examScheduleID = AttentedTester.ScheduleId
	
	LEFT OUTER JOIN 
	(
		SELECT *
			FROM
			(
				SELECT examScheduleID AS ScheduleId, examSubmitStatus AS SubmitStatus, COUNT(examineeID) AS TotalPerson
					FROM userexam
					GROUP BY examScheduleID, examSubmitStatus			
			 ) AS SubmitDetail
			 PIVOT(SUM(TotalPerson) FOR SubmitStatus IN([User Submited], [Auto Submited], [Colse Window Submit], [Running], [Disconnected])) AS SubmitDetail
	 ) AS ExamSubmitDetail ON aSchedule.examScheduleID = ExamSubmitDetail.ScheduleId
	
			
	WHERE aSchedule.examScheduleID = (SELECT userexam.examScheduleID
										FROM userexam
											 INNER JOIN [user] ON userexam.userID = [user].userID
											 INNER JOIN examschedule ON userexam.examScheduleID = examschedule.examScheduleID
											 INNER JOIN exam ON examschedule.examID = exam.examID
										WHERE [user].userFirstName = 'Ishrat Fatima'
											AND exam.examCode = 'CLJUN13Q1');
											





------------------- User Wise Exam Detail =>
WITH ExamDetail
AS
(
	SELECT 
		   examschedule.examScheduleID AS ScheduleId, examschedule.examScheduleCode AS ScheduleCode,
		   exam.examID AS ExamId, exam.examCode AS ExamCode, exam.isQuestionTimeConsiderable AS IsQuestionTimeConsiderable, 
		   unit.unitID AS UnitId, unit.unitName AS UnitName, unit.unitCode AS UnitCode,
		   
		   ----------- Time Duretions =>
		   examschedule.examScheduleGracePeriod AS [ScheduleGracePeriod(mm)],
		   exam.examDuration AS [ExamDuration(mm)],
		   exam.examGracePeriod AS [ExamGracePeriod(mm)],
		   ExamQuestionLevel.[QuestionLevelDuration(ss)],
		   
		   -------- Total Time
		   CONVERT
		   (VARCHAR,
			   DATEADD
			   (S,
				   CASE WHEN (exam.isQuestionTimeConsiderable = 'no')   THEN( (examschedule.examScheduleGracePeriod+exam.examDuration+exam.examGracePeriod)*60 )
						WHEN (exam.isQuestionTimeConsiderable = 'yes')  THEN( ExamQuestionLevel.[QuestionLevelDuration(ss)] )
						END,
				'19700101'
			   ),
			108
		   )AS ExamTotalTime
		   
		FROM examschedule
		INNER JOIN exam ON examschedule.examID = exam.examID
		INNER JOIN unit ON exam.unitID = unit.unitID
		INNER JOIN
		------- Question Level Time for an exam 
		(
			SELECT questionsexam.examID AS ExamId, SUM(question.questionDurationSec) AS [QuestionLevelDuration(ss)]
				FROM questionsexam
				INNER JOIN question ON questionsexam.questionID = question.questionID
				WHERE question.isActive = 'yes'
					AND question.isDeleted = 'no'
				GROUP BY questionsexam.examID
				
		) AS ExamQuestionLevel ON exam.examID = ExamQuestionLevel.ExamId
)
SELECT 
	userexam.userID,
	ExamDetail.UnitName,
	userexam.examineeMarksPercentage,
	CONVERT(VARCHAR, userexam.examFinished-userexam.examStarted, 108) AS UserConsignedTime,
	CONVERT(VARCHAR(10), userexam.examStarted, 103) + ' ' + RIGHT(CONVERT(VARCHAR, userexam.examStarted, 100), 7) AS ExamStarted,
	CONVERT(VARCHAR(10), userexam.examFinished, 103) + ' ' + RIGHT(CONVERT(VARCHAR, userexam.examFinished, 100), 7) AS ExamFinished,
	ExamDetail.ExamTotalTime, 
	userexam.examTotalMark,
	userexam.examineeTotalObtainedMarks
	FROM userexam
	LEFT OUTER JOIN ExamDetail ON userexam.examScheduleID = ExamDetail.ScheduleId
	WHERE userexam.examSubmitStatus = 'Colse Window Submit'
		AND userexam.examScheduleID = 33
------------------- User Wise Exam Detail <=	
