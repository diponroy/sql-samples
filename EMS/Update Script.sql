/***************    TO CLEAN UNUSED DATA FROM DATABASE   *********************/

DELETE FROM userexam
DELETE FROM userexamquestion
DELETE FROM emailsettings
DELETE FROM examinergroup
DELETE FROM examinergroup_histroy
DELETE FROM examschedule
DELETE FROM gradetype
DELETE FROM question
DELETE FROM questionsexam
DELETE FROM tempExamSchedule
DELETE FROM tempQuestionAnswerOptions
DELETE FROM tempquestionsexam
DELETE FROM [user] WHERE userID <> 1
DELETE FROM user_usergroup
DELETE FROM user_userrole
DELETE FROM usergroup WHERE userGroupID <> 1
DELETE FROM usergroup_userrole
DELETE FROM userrole
DELETE FROM unit WHERE unitID <> 1
DELETE FROM exam
UPDATE unit SET createdBy = 1 WHERE unitID = 1
DELETE FROM tempUserSheduledExamQuestion







DELETE FROM permission WHERE permissionID > 18
UPDATE permission SET permissionName = 'Email Header Footer' WHERE permissionID = 1
UPDATE permission SET permissionName = 'Exam Schedule' WHERE permissionID = 2
UPDATE permission SET permissionName = 'Exam Manage' WHERE permissionID = 3
UPDATE permission SET permissionName = 'Question Manage' WHERE permissionID = 4
UPDATE permission SET permissionName = 'Question Type' WHERE permissionID = 5
UPDATE permission SET permissionName = 'Grade System' WHERE permissionID = 6
UPDATE permission SET permissionName = 'Grade Type' WHERE permissionID = 7
UPDATE permission SET permissionName = 'Role' WHERE permissionID = 8
UPDATE permission SET permissionName = 'Permission' WHERE permissionID = 9
UPDATE permission SET permissionName = 'User Group' WHERE permissionID = 10
UPDATE permission SET permissionName = 'User Manage' WHERE permissionID = 11
UPDATE permission SET permissionName = 'User Type' WHERE permissionID = 12
UPDATE permission SET permissionName = 'Unit' WHERE permissionID = 13
UPDATE permission SET permissionName = 'Examiner Group' WHERE permissionID = 14
UPDATE permission SET permissionName = 'Test Wise Exam' WHERE permissionID = 15
UPDATE permission SET permissionName = 'Question Wise Exam' WHERE permissionID = 16
UPDATE permission SET permissionName = 'Send Email' WHERE permissionID = 17
UPDATE permission SET permissionName = 'Publish Result' WHERE permissionID = 18
INSERT INTO permission(permissionName) VALUES ('Assesment')
INSERT INTO permission(permissionName) VALUES ('Assign Permission')
INSERT INTO permission(permissionName) VALUES ('View Permission')


ALTER TABLE dbo.[user] ALTER COLUMN userPassword varchar(100) NULL
ALTER TABLE exam ALTER COLUMN examNegativeMarkingPercentage int NULL
ALTER TABLE exam ALTER COLUMN examGracePeriod int NULL
ALTER TABLE exam ADD CONSTRAINT DF_examNegativeMarkingPercentage DEFAULT 0 FOR examNegativeMarkingPercentage
ALTER TABLE exam ADD CONSTRAINT DF_examGracePeriod DEFAULT 0 FOR examGracePeriod












ALTER TABLE dbo.[userrole] ADD roleIsDeleted varchar(5) NULL
ALTER TABLE dbo.[userrole] ADD  CONSTRAINT [DF_userrole_roleIsDeleted]  DEFAULT ('no') FOR [roleIsDeleted]
ALTER TABLE dbo.[userrole] ADD created datetime NULL
ALTER TABLE dbo.[userrole] ADD lastModified datetime NULL
ALTER TABLE dbo.[userrole] ADD createdBy int NULL
ALTER TABLE dbo.[userrole] ADD modifiedBy int NULL
ALTER TABLE dbo.[userrole] ADD deletedBy int NULL

ALTER TABLE dbo.[gradetype] ADD gradeIsDeleted varchar(5) NULL
ALTER TABLE dbo.[gradetype] ADD  CONSTRAINT [DF_gradetype_gradeIsDeleted]  DEFAULT ('no') FOR [gradeIsDeleted]
ALTER TABLE dbo.[gradetype] ADD created datetime NULL
ALTER TABLE dbo.[gradetype] ADD lastModified datetime NULL
ALTER TABLE dbo.[gradetype] ADD createdBy int NULL
ALTER TABLE dbo.[gradetype] ADD modifiedBy int NULL
ALTER TABLE dbo.[gradetype] ADD deletedBy int NULL

UPDATE dbo.[userrole] SET roleIsDeleted = 'no'
UPDATE dbo.[gradetype] SET gradeIsDeleted = 'no'

INSERT INTO permission(permissionName) VALUES ('Send Result')







ALTER TABLE dbo.[unit] ALTER COLUMN unitLogo varchar(200) NULL
ALTER TABLE dbo.[questionimage] ALTER COLUMN imageName varchar(200) NULL
ALTER TABLE dbo.[user] ALTER COLUMN userImage varchar(200) NULL




ALTER TABLE dbo.[user] ALTER COLUMN userAddress TEXT NULL
ALTER TABLE dbo.[user] ALTER COLUMN userSignature VARCHAR(100) NULL
ALTER TABLE dbo.[user] ALTER COLUMN userReportMsg VARCHAR(100) NULL
ALTER TABLE dbo.[user] ALTER COLUMN userMobile VARCHAR(15) NULL
ALTER TABLE dbo.[user] ALTER COLUMN userLastName VARCHAR(30) NULL




/*************** Dipon : 25.02.2013, New Clear Scripts *********************/

DELETE FROM userexam
DELETE FROM userexamquestion
DELETE FROM emailsettings
DELETE FROM examinergroup
DELETE FROM examinergroup_histroy
DELETE FROM examschedule
DELETE FROM gradetype
DELETE FROM question
DELETE FROM questionsexam
DELETE FROM tempExamSchedule
DELETE FROM tempQuestionAnswerOptions
DELETE FROM tempquestionsexam
DELETE FROM [user] WHERE userID <> 1
DELETE FROM user_usergroup
DELETE FROM user_userrole
DELETE FROM usergroup 
DELETE FROM usergroup_userrole
DELETE FROM userrole
DELETE FROM unit 
DELETE FROM exam
UPDATE unit SET createdBy = 1 WHERE unitID = 1
DELETE FROM tempUserSheduledExamQuestion
DELETE FROM mailSendingHistory
DELETE FROM userexam_history
DELETE FROM userexamquestion_history
DELETE FROM userexamquestiondetails_history
DELETE FROM tempUserSheduledExamQuestion_history
DELETE FROM tempMailSendingUsers
DELETE FROM tempExaminerGroup

/********************************  End  **************************************/





/*************** Dipon : 25.02.2013, Show all database tbls and row counts *********************/

GO
SELECT table_name from information_schema.tables


GO
SELECT 
    [TableName] = so.name, 
    [RowCount] = MAX(si.rows) 
FROM 
    sysobjects so, 
    sysindexes si 
WHERE 
    so.xtype = 'U' 
    AND 
    si.id = OBJECT_ID(so.name) 
GROUP BY 
    so.name 
ORDER BY 
    2 DESC



GO
SELECT sc.name +'.'+ ta.name TableName, SUM(pa.rows) RowCnt
 FROM sys.tables ta
 INNER JOIN sys.partitions pa
 ON pa.OBJECT_ID = ta.OBJECT_ID
 INNER JOIN sys.schemas sc
 ON ta.schema_id = sc.schema_id
 WHERE ta.is_ms_shipped = 0 AND pa.index_id IN (1,0)
 GROUP BY sc.name,ta.name
 ORDER BY ta.name , SUM(pa.rows) DESC
 
 


GO
SELECT  SchemaName = schemas.[name],
        TableName = tables.[name],
        IndexName = indexes.[name],
        IndexType =
            CASE indexes.type
                WHEN 0 THEN 'Heap'
                WHEN 1 THEN 'Clustered'
            END,
        IndexPartitionCount = partition_info.PartitionCount,
        IndexTotalRows = partition_info.TotalRows
FROM    sys.tables
        JOIN sys.indexes
            ON  tables.object_id = indexes.object_id
                AND indexes.type IN ( 0, 1 )
        JOIN (  SELECT object_id, index_id, PartitionCount = COUNT(*), TotalRows = SUM(rows)
                FROM sys.partitions
                GROUP BY object_id, index_id
        ) partition_info
            ON  indexes.object_id = partition_info.object_id
                AND indexes.index_id = partition_info.index_id
        JOIN sys.schemas ON tables.schema_id = schemas.schema_id
ORDER BY SchemaName, TableName;


//202CB962AC59075B964B07152D234B70==123
//E10ADC3949BA59ABBE56E057F20F883E == 123456

/********************************  End  **************************************/





/***************   UPDATE SCRIPT FOR view_ExaminerWithoutHierarchy *********************/
GO
AlTER VIEW view_ExaminerWithoutHierarchy
AS
SELECT     aUserExam.userID AS ExamineeId, aUserExam.examStatus AS ExamStatus, aUserExam.examScheduleID AS ExamSheduleId, CONVERT(VARCHAR(10), aUserExam.examStarted, 103) 
                      + ' ' + RIGHT(CONVERT(VARCHAR, aUserExam.examStarted, 100), 7) AS ExamStarted, CONVERT(VARCHAR(10), aUserExam.examFinished, 103) + ' ' + RIGHT(CONVERT(VARCHAR, 
                      aUserExam.examFinished, 100), 7) AS ExamFinished, aExaminee.userFirstName AS ExamineeFirstName, aExaminee.userLastName AS ExamineeLastName, aExam.examID AS ExamId, aExam.examTitle AS ExamTitle, 
                      aExam.examNumberofQuestionPerPage AS QsnPerPage, aExam.examTotalQuestionNumber AS TotalQsn, aUnit.unitID AS UnitId, aUnit.unitName AS UnitName, aUnit.isDeleted AS UnitIsDeleted, aExam.examIsActive AS ExamIsActive, 
                      aExam.examIsDeleted AS ExamIsDeleted, aExamShedule.examScheduleIsActive AS SheduleIsActive, aExamShedule.examScheduleIsDeleted AS SheduleIsDeleted, aExaminee.userIsActive AS ExamineeIsActive, 
                      aExaminee.userIsDeleted AS ExamineeIsDeleted, aUserExam.examStarted AS Started, aUserExam.examFinished AS Finished, CONVERT(VARCHAR, CONVERT(DATETIME, aUserExam.examFinished - aUserExam.examStarted), 108 )   
                      AS TimeDifference, aUserExam.examSubmitStatus AS ExamSubmitStatus
FROM         dbo.userexam AS aUserExam INNER JOIN
                      dbo.[user] AS aExaminee ON aUserExam.userID = aExaminee.userID INNER JOIN
                      dbo.examschedule AS aExamShedule ON aUserExam.examScheduleID = aExamShedule.examScheduleID INNER JOIN
                      dbo.exam AS aExam ON aExamShedule.examID = aExam.examID INNER JOIN
                      dbo.user_unit AS aUserUnit ON aExaminee.userID = aUserUnit.userID INNER JOIN
                      dbo.unit AS aUnit ON aExam.unitID = aUnit.unitID AND aUserUnit.unitID = aUnit.unitID
/***************  END   *********************/





/***************   UPDATE SCRIPT FOR view_UserExamExaminerHierarchy   *********************/
GO
ALTER VIEW view_UserExamExaminerHierarchy
AS
WITH n(CHILD, PARENT, GENERATION, hierarchy) AS (SELECT     examinerID, parentID, 0 AS Expr1, CAST(examinerID AS nvarchar) AS GENERATION
                                                                                                                     FROM          dbo.examinergroup
                                                                                                                     WHERE      (parentID = 0) AND (isDeleted = 'no')
                                                                                                                     UNION ALL
                                                                                                                     SELECT     nplus1.examinerID, nplus1.parentID, n_2.GENERATION + 1 AS Expr1, CAST(n_2.hierarchy + ').(' + CAST(nplus1.examinerID AS nvarchar) 
                                                                                                                                           AS nvarchar) AS Expr2
                                                                                                                     FROM         dbo.examinergroup AS nplus1 INNER JOIN
                                                                                                                                           n AS n_2 ON nplus1.parentID = n_2.CHILD
                                                                                                                     WHERE     (nplus1.isDeleted = 'no'))
    SELECT     n_1.CHILD, n_1.PARENT, n_1.GENERATION, n_1.hierarchy, examinergroup_1.examinerGroupID, UE.examineeID AS Id, u.userFirstName AS ExamineeFirstName, 
                            u.userLastName AS ExamineeLastName, exmr.userFirstName AS ExaminerFirstName, exmr.userLastName AS ExaminerLastName, dbo.unit.unitID AS UnitId, dbo.unit.unitName AS UnitName, e.examID AS ExamId, e.examTitle AS ExamTitle, 
                            es.examScheduleID AS ScheduleId, u.userID AS ExamineeId, exmr.userID AS ExaminerId, e.examNumberofQuestionPerPage AS QuestionPerPage, 
                            e.examTotalQuestionNumber AS TotalQuestions, UE.examStatus, e.examCode, es.examScheduleStartDateTime, es.examScheduleEndDateTime, es.examScheduleStartTime, 
                            es.examScheduleEndTime, es.examScheduleIsActive, es.examScheduleIsDeleted, e.examIsActive, e.examIsDeleted, u.userIsActive, u.userIsDeleted, 
                            examinergroup_1.isDeleted AS examinerGrpIsDeleted, CONVERT(VARCHAR(10), UE.examStarted, 103) + ' ' + RIGHT(CONVERT(VARCHAR, UE.examStarted, 100), 7) AS examStarted, 
                            CONVERT(VARCHAR(10), UE.examFinished, 103) + ' ' + RIGHT(CONVERT(VARCHAR, UE.examFinished, 100), 7) AS examFinished, CONVERT(VARCHAR, CONVERT(DATETIME, UE.examFinished - UE.examStarted), 108)
                            AS TimeDifference, UE.examSubmitStatus AS ExamSubmitStatus
     FROM         n AS n_1 INNER JOIN
                            dbo.examinergroup AS examinergroup_1 ON n_1.CHILD = examinergroup_1.examinerID INNER JOIN
                            dbo.user_examinergroup AS uexrg ON examinergroup_1.examinerGroupID = uexrg.examinerGroupID INNER JOIN
                            dbo.[user] AS exmr ON uexrg.examinerID = exmr.userID INNER JOIN
                            dbo.[user] AS u INNER JOIN
                            dbo.user_unit AS uu ON u.userID = uu.userID INNER JOIN
                            dbo.userexam AS UE ON uu.userID = UE.userID AND uu.unitID = UE.unitID INNER JOIN
                            dbo.examschedule AS es ON UE.examScheduleID = es.examScheduleID INNER JOIN
                            dbo.exam AS e ON es.examID = e.examID INNER JOIN
                            dbo.unit ON uu.unitID = dbo.unit.unitID ON uexrg.userID = u.userID

/***************  END   *********************/




/***************   Dipon 28.01.2013 : ALTER userExam *********************/
GO
ALTER TABLE userexam
	ADD examTotalMark FLOAT NULL
GO
ALTER TABLE userexam
	ADD examineeGrade VARCHAR(10) NULL
GO
ALTER TABLE userexam
	ADD examineeGradeDescription TEXT NULL
GO
ALTER TABLE userexam
	ADD gradeStartMarks FLOAT NULL
GO
ALTER TABLE userexam
	ADD gradeEndMarks FLOAT NULL
GO
ALTER TABLE userexam
	ADD examineeMarksPercentage FLOAT NULL

/******************************   END   *********************************/




/*********************************************   Dipon 29.01.2013 : New Tables ***********************************************/
/****** Object:  Table [dbo].[userexamquestiondetails_history]    Script Date: 01/29/2013 15:03:30 ******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userexamquestiondetails_history](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[examineeAnswerDetailID] [int] NOT NULL,
	[examineeAnswerID] [int] NOT NULL,
	[questionID] [int] NOT NULL,
	[questionAnswerID] [int] NULL,
	[examineeAnswerValue] [varchar](250) NOT NULL,
	[examScheduleID] [int] NOT NULL,
	[examineeAnswerStatus] [varchar](5) NULL,
	[examID] [int] NOT NULL,
 CONSTRAINT [PK_userexamquestiondetails_history] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userexamquestion_history]    Script Date: 01/29/2013 15:03:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userexamquestion_history](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[examineeAnswerID] [int] NOT NULL,
	[questionID] [int] NOT NULL,
	[examScheduleID] [int] NOT NULL,
	[examineeAnswerStatus] [varchar](5) NOT NULL,
	[examineeAnswerObtainedMark] [float] NOT NULL,
	[examID] [int] NOT NULL,
	[userID] [int] NOT NULL,
 CONSTRAINT [PK_userexamquestion_history] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[userexam_history]    Script Date: 01/29/2013 15:03:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[userexam_history](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[examineeID] [int] NOT NULL,
	[userID] [int] NOT NULL,
	[examScheduleID] [int] NOT NULL,
	[examineeStatus] [varchar](50) NOT NULL,
	[examineeLogin] [varchar](30) NULL,
	[examineePassword] [varchar](100) NULL,
	[examStatus] [varchar](50) NOT NULL,
	[examineeTotalObtainedMarks] [float] NOT NULL,
	[unitID] [int] NOT NULL,
	[isDeleted] [varchar](5) NOT NULL,
	[created] [datetime] NOT NULL,
	[lastModified] [datetime] NULL,
	[createdBy] [int] NOT NULL,
	[modifiedBy] [int] NULL,
	[deletedBy] [int] NULL,
	[isActive] [varchar](5) NULL,
	[examStarted] [datetime] NULL,
	[examFinished] [datetime] NULL,
	[examTotalMark] [float] NULL,
	[examineeGrade] [varchar](10) NULL,
	[examineeGradeDescription] [text] NULL,
	[gradeStartMarks] [float] NULL,
	[gradeEndMarks] [float] NULL,
	[examineeMarksPercentage] [float] NULL,
 CONSTRAINT [PK_userexam_history] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tempUserSheduledExamQuestion_history]    Script Date: 01/29/2013 15:03:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tempUserSheduledExamQuestion_history](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[sheduleId] [int] NOT NULL,
	[examId] [int] NOT NULL,
	[questionId] [int] NOT NULL,
	[sheduledExamQuestionId] [int] NOT NULL,
 CONSTRAINT [PK_tempUserSheduledExamQuestion_history] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_userexam_history_isDeleted]    Script Date: 01/29/2013 15:03:30 ******/
ALTER TABLE [dbo].[userexam_history] ADD  CONSTRAINT [DF_userexam_history_isDeleted]  DEFAULT ('no') FOR [isDeleted]
GO
/****** Object:  Default [DF_userexam_history_isActive]    Script Date: 01/29/2013 15:03:30 ******/
ALTER TABLE [dbo].[userexam_history] ADD  CONSTRAINT [DF_userexam_history_isActive]  DEFAULT ('yes') FOR [isActive]
/*********************************************   END ***********************************************/





/*********************************************   Dipon 30.01.2013 : Add description field to ***********************************************/

GO
ALTER TABLE question
	ADD questionDescription TEXT NULL	
GO
UPDATE 
	question
	SET question.questionDescription = question.questionTitle	
GO
ALTER TABLE question
	ALTER COLUMN questionDescription TEXT NOT NULL
GO
ALTER TABLE question
	ALTER COLUMN questionDescription TEXT NULL

/*********************************************   END ***********************************************/




/*********************************************   Dipon 30.01.2013 : Add description field to ***********************************************/
GO
ALTER Table userexam
	ADD examSubmitStatus VARCHAR(50) NULL
GO
ALTER Table userexam_history 
	ADD examSubmitStatus VARCHAR(50) NULL
/*********************************************   END ***********************************************/




/******* Dipon 11.02.2013 : Add userGrpHrcy and examinerHrcy to userGroup, examinerGroup **********/
GO
ALTER TABLE dbo.usergroup
	ADD userGroupHrcy VARCHAR(1000) NULL
GO	
ALTER TABLE dbo.examinergroup
	ADD examinerHrcy VARCHAR(1000) NULL
GO
ALTER TABLE dbo.examinergroup_histroy
	ADD examinerHrcy VARCHAR(1000) NULL
GO
ALTER TABLE tempExamSchedule
	ADD tempGroupId INT DEFAULT(0) NOT NULL 
GO
UPDATE usergroup
	SET userGroupHrcy = '.' + SUBSTRING(aUserGrpHrchy.hierarchy, PATINDEX('%[0-9]%', aUserGrpHrchy.hierarchy), LEN(aUserGrpHrchy.hierarchy))
	FROM view_UserGroupHierarchy AS aUserGrpHrchy
		WHERE aUserGrpHrchy.userGroupID = usergroup.userGroupID
GO
UPDATE examinergroup
	SET examinerHrcy = '.' +SUBSTRING(aExaminerHrchy.hierarchy, PATINDEX('%[0-9]%', aExaminerHrchy.hierarchy), LEN(aExaminerHrchy.hierarchy))
	FROM view_ExaminerGroupHierarchy AS aExaminerHrchy
	WHERE aExaminerHrchy.examinerGroupID = examinergroup.examinerGroupID
GO
UPDATE examinergroup_histroy
	SET examinerHrcy = '.' +SUBSTRING(aExaminerHrchy.hierarchy, PATINDEX('%[0-9]%', aExaminerHrchy.hierarchy), LEN(aExaminerHrchy.hierarchy))
	FROM view_ExaminerGroupHierarchy AS aExaminerHrchy
	WHERE (aExaminerHrchy.CHILD = examinergroup_histroy.examinerID)
		AND (aExaminerHrchy.PARENT = examinergroup_histroy.parentID)

GO
WITH UserExamResult
AS
(
	SELECT aUserExam.examineeID, aGrade.gradeName, aGrade.gradeDescription, aGrade.gradeStartMarks, aGrade.gradeEndMarks, (aUserExam.examineeTotalObtainedMarks/aExam.examTotalMarks*100) AS examineeMarksPercentage, 'User Submited' AS examSubmitStatus,
		(CASE WHEN(aExam.isQuestionMarkConsiderable = 'no') 
				THEN(aExam.examTotalMarks)
			ELSE(
					SELECT SUM(aQsn.questionWeight)
						FROM tempUserSheduledExamQuestion AS aTmp
						JOIN question AS aQsn ON aTmp.questionId = aQsn.questionID
						WHERE aTmp.userId = aUserExam.userID
							AND aTmp.sheduleId = aUserExam.examScheduleID
							AND aTmp.examId = aExam.examID
				)
			END) AS examTotalMarks		
		FROM userexam AS aUserExam
		LEFT JOIN examschedule AS aSchedule ON aUserExam.examScheduleID = aSchedule.examScheduleID
		LEFT JOIN exam AS aExam ON aSchedule.examID = aExam.examID
		LEFT JOIN grade AS aGrade ON ( (aGrade.gradeStartMarks <= aUserExam.examineeTotalObtainedMarks/aExam.examTotalMarks*100 AND aUserExam.examineeTotalObtainedMarks/aExam.examTotalMarks*100 <= aGrade.gradeEndMarks) AND aExam.gradeTypeID = aGrade.gradeTypeID)
)
UPDATE userexam
	SET examTotalMark = aResult.examTotalMarks,
		examineeGrade = aResult.gradeName,
		examineeGradeDescription = aResult.gradeDescription,
		gradeStartMarks = aResult.gradeStartMarks,
		gradeEndMarks = aResult.gradeEndMarks,
		examineeMarksPercentage = aResult.examineeMarksPercentage,
		examSubmitStatus = aResult.examSubmitStatus
	FROM UserExamResult AS aResult, userexam AS aUserExam
	WHERE aResult.examineeID = aUserExam.examineeID

/*********************************************   END ***********************************************/





/******* Dipon 14.02.2013 : Add Temp ExaminerGroup tbl **********/
GO
CREATE TABLE [dbo].[tempExaminerGroup](
	[tempId] [int] IDENTITY(1,1) NOT NULL,
	[tempExaminerGoupId] [int] NOT NULL,
	[tempUnitId] [int] NOT NULL,
	[tempUserId] [int] NOT NULL,
	[tempGroupId] [int] NOT NULL,
 CONSTRAINT [PK_tempExaminerGroup] PRIMARY KEY CLUSTERED 
(
	[tempId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tempExaminerGroup] ADD  DEFAULT ((0)) FOR [tempGroupId]

/*********************************************   END ***********************************************/





/******* Dipon 17.02.2013 : Add Temp ExaminerGroup tbl **********/
GO
ALTER TABLE dbo.mailSendingHistory
	Add messageBody VARCHAR(1000) NULL
GO
UPDATE dbo.mailSendingHistory
	SET messageBody = dbo.mailSendingHistory.mailType
GO
ALTER TABLE dbo.mailSendingHistory
	 ALTER COLUMN messageBody VARCHAR(1000)NOT NULL
/*********************************************   END ***********************************************/




/******* Dipon 25.02.2013 : Add a Permission to permission Tbl **********/
GO
INSERT 
	INTO permission(permissionName)
	VALUES ('Assesment History') 

/*********************************************   END ***********************************************/




/******* Dipon 25.02.2013 : for historyTbl Show **********/
GO
CREATE VIEW [dbo].[view_ExaminerWithoutHierarchy_History]
AS	
SELECT     aUserExam.userID AS ExamineeId, aUserExam.examStatus AS ExamStatus, aUserExam.examScheduleID AS ExamSheduleId, CONVERT(VARCHAR(10), aUserExam.examStarted, 103) 
                      + ' ' + RIGHT(CONVERT(VARCHAR, aUserExam.examStarted, 100), 7) AS ExamStarted, CONVERT(VARCHAR(10), aUserExam.examFinished, 103) + ' ' + RIGHT(CONVERT(VARCHAR, 
                      aUserExam.examFinished, 100), 7) AS ExamFinished, aExaminee.userFirstName AS ExamineeFirstName, aExaminee.userLastName AS ExamineeLastName, aExam.examID AS ExamId, aExam.examTitle AS ExamTitle, 
                      aExam.examNumberofQuestionPerPage AS QsnPerPage, aExam.examTotalQuestionNumber AS TotalQsn, aUnit.unitID AS UnitId, aUnit.unitName AS UnitName, aUnit.isDeleted AS UnitIsDeleted, aExam.examIsActive AS ExamIsActive, 
                      aExam.examIsDeleted AS ExamIsDeleted, aExamShedule.examScheduleIsActive AS SheduleIsActive, aExamShedule.examScheduleIsDeleted AS SheduleIsDeleted, aExaminee.userIsActive AS ExamineeIsActive, 
                      aExaminee.userIsDeleted AS ExamineeIsDeleted, aUserExam.examStarted AS Started, aUserExam.examFinished AS Finished, CONVERT(VARCHAR, CONVERT(DATETIME, aUserExam.examFinished - aUserExam.examStarted), 108 )   
                      AS TimeDifference, aUserExam.examSubmitStatus AS ExamSubmitStatus
FROM         dbo.userexam_history AS aUserExam INNER JOIN
                      dbo.[user] AS aExaminee ON aUserExam.userID = aExaminee.userID INNER JOIN
                      dbo.examschedule AS aExamShedule ON aUserExam.examScheduleID = aExamShedule.examScheduleID INNER JOIN
                      dbo.exam AS aExam ON aExamShedule.examID = aExam.examID INNER JOIN
                      dbo.user_unit AS aUserUnit ON aExaminee.userID = aUserUnit.userID INNER JOIN
                      dbo.unit AS aUnit ON aExam.unitID = aUnit.unitID AND aUserUnit.unitID = aUnit.unitID
/*********************************************   END ***********************************************/




/******* Dipon 27.02.2013 : for user salutation **********/
GO
ALTER TABLE dbo.[user]
	ALTER COLUMN salutationID INT NULL
GO
ALTER TABLE dbo.[user]
	ADD  DEFAULT ((0)) FOR [salutationID]
/*********************************************   END ***********************************************/





/******* Dipon 06.03.2013 : Add a Permission to permission Tbl **********/
GO
INSERT 
	INTO permission(permissionName)
	VALUES ('User Wise Exam') 

/*********************************************   END ***********************************************/





/******* Dipon 24.03.2013 : update the length of the firstName of the user table **********/
GO
ALTER TABLE [dbo].[user]
	ALTER COLUMN [userFirstName] [varchar](200) NOT NULL
/*********************************************   END ***************************************/




/******* Dipon 15.05.2013 : Add order to the questions && Defalut 0 **********/

-- for tempUserSheduledExamQuestion_history
GO
ALTER TABLE tempUserSheduledExamQuestion_history
	ADD questionOrder INT NULL
GO
ALTER TABLE tempUserSheduledExamQuestion_history
	ADD DEFAULT(0) FOR questionOrder
GO
UPDATE tempUserSheduledExamQuestion_history
	SET questionOrder = 0


-- for tempUserSheduledExamQuestion
GO
ALTER TABLE tempUserSheduledExamQuestion
	ADD questionOrder INT NULL
GO
ALTER TABLE tempUserSheduledExamQuestion
	ADD DEFAULT(0) FOR questionOrder
GO
UPDATE tempUserSheduledExamQuestion
	SET questionOrder = 0


-- for questionsexam
GO
ALTER TABLE questionsexam
	ADD questionOrder INT NULL
GO
ALTER TABLE questionsexam
	ADD DEFAULT(0) FOR questionOrder
GO
UPDATE questionsexam
	SET questionOrder = 0


-- for tempquestionsexam
GO
ALTER TABLE tempquestionsexam
	ADD questionOrder INT NULL
GO
ALTER TABLE tempquestionsexam
	ADD DEFAULT(0) FOR questionOrder
GO
UPDATE tempquestionsexam
	SET questionOrder = 0
/*********************************************   END ***********************************************/


	