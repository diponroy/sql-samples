DECLARE @tableNameList VARCHAR(1000)
SET @tableNameList = 'Student, Department, Student_Course_Link, Course, University, Teacher, Course_Teacher_Link';
	
DECLARE @tblQueryDetail TABLE(Name VARCHAR(100) NOT NULL,
							  Generation INT NULL,
							  HasRelation BIT DEFAULT(0),
							  IsChecked BIT DEFAULT(0),
							  FirstTableName VARCHAR(100) NULL, 
							  FirstTableColumnName VARCHAR(100) NULL, 
							  RefTableName VARCHAR(100) NULL, 
							  RefTableColumnName VARCHAR(100) NULL)
DECLARE @aTblName VARCHAR(100)
DECLARE @index INT
SET @index = 1;

-- get table name from string to table name Table
WHILE (CHARINDEX(',', @tableNameList) > 0)
BEGIN
	SET @index = CHARINDEX(',', @tableNameList);
	SET @aTblName = LTRIM(RTRIM(SUBSTRING(@tableNameList, 1, @index-1))); 
	INSERT INTO @tblQueryDetail(Name) VALUES(@aTblName)
	SET @tableNameList = LTRIM(RTRIM(SUBSTRING(@tableNameList, @index+1, LEN(@tableNameList))));
	SET @index = CHARINDEX(',', @tableNameList);
END
IF (LEN(@tableNameList) > 0)
BEGIN
	SET @aTblName = LTRIM(RTRIM(SUBSTRING(@tableNameList, @index+1, LEN(@tableNameList))));
	INSERT INTO @tblQueryDetail(Name) VALUES(@aTblName) 
	SET @tableNameList = '';
END

DECLARE @totalRows int
DECLARE @mainTable VARCHAR(100)

SET @totalRows = (SELECT COUNT(Name)
					FROM @tblQueryDetail);
SET @mainTable = (SELECT TOP(1) Name 
					FROM @tblQueryDetail);

--------------------------- Show values --------------------------
--SELECT *
--	FROM @tblQueryDetail 
--SELECT @totalRows AS [Total Table]
--SELECT @mainTable AS [Main Table]

--SELECT f.name AS ForeignKey,
--	SCHEMA_NAME(f.SCHEMA_ID) SchemaName,
--	OBJECT_NAME(f.parent_object_id) AS TableName,
--	COL_NAME(fc.parent_object_id,fc.parent_column_id) AS ColumnName,
--	SCHEMA_NAME(o.SCHEMA_ID) ReferenceSchemaName,
--	OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,
--	COL_NAME(fc.referenced_object_id,fc.referenced_column_id) AS ReferenceColumnName
--FROM sys.foreign_keys AS f
--INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id
--INNER JOIN sys.objects AS o ON o.OBJECT_ID = fc.referenced_object_id
--WHERE CAST(OBJECT_NAME(f.parent_object_id) AS VARCHAR)
--		--IN('Student', 'Department', 'Course', 'Student_Course_Link', 'University', 'Teacher', 'Course_Teacher_Link')
--		IN(SELECT Name FROM @tblQueryDetail)
--	AND CAST(OBJECT_NAME (f.referenced_object_id) AS VARCHAR)
--		--IN('Student', 'Department', 'Course', 'Student_Course_Link', 'University', 'Teacher', 'Course_Teacher_Link')
--		IN(SELECT Name FROM @tblQueryDetail)
--ORDER BY f.name
-------------------

DECLARE @tblTableRelation TABLE(TableName VARCHAR(100),
								ColumnName VARCHAR(100),
								ReferenceTableName VARCHAR(100),
								ReferenceColumnName VARCHAR(100))

-- Get table relations FK wise
INSERT 
	INTO @tblTableRelation (TableName, ColumnName, ReferenceTableName, ReferenceColumnName)
	( 
		SELECT 
				OBJECT_NAME(f.parent_object_id) AS TableName,
				COL_NAME(fc.parent_object_id,fc.parent_column_id) AS ColumnName,
				OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,
				COL_NAME(fc.referenced_object_id,fc.referenced_column_id) AS ReferenceColumnName
			FROM sys.foreign_keys AS f
			INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id
			INNER JOIN sys.objects AS o ON o.OBJECT_ID = fc.referenced_object_id
			WHERE CAST(OBJECT_NAME(f.parent_object_id) AS VARCHAR)
						--IN('Student', 'Department', 'Course', 'Student_Course_Link', 'University', 'Teacher', 'Course_Teacher_Link')
						IN(SELECT Name FROM @tblQueryDetail)
				  AND CAST(OBJECT_NAME (f.referenced_object_id) AS VARCHAR)
						--IN('Student', 'Department', 'Course', 'Student_Course_Link', 'University', 'Teacher', 'Course_Teacher_Link')
						IN(SELECT Name FROM @tblQueryDetail)
	)

DECLARE @crntMainTbl VARCHAR(100)
DECLARE @crntGeneration INT
DECLARE @fstTblNm VARCHAR(100)
DECLARE @fstTblClmNm VARCHAR(100)
DECLARE @refTblNm VARCHAR(100)
DECLARE @refTblClmNm VARCHAR(100)

UPDATE @tblQueryDetail
	SET Generation = 0,
	HasRelation = (SELECT CASE WHEN(COUNT(TableName) > 0)
								THEN(1) ELSE(0)END
							FROM @tblTableRelation
							WHERE ReferenceTableName = @mainTable
								OR TableName = @mainTable) 
	WHERE Name = @mainTable


--Construct the tblQueryDetail
WHILE((SELECT COUNT(Name) 
		FROM @tblQueryDetail
		WHERE HasRelation = 1
			AND IsChecked = 0) > 0)
BEGIN
	SET @crntMainTbl = (SELECT TOP(1) Name
							FROM @tblQueryDetail
							WHERE HasRelation = 1
								AND IsChecked = 0);
	SET @crntGeneration = (SELECT Generation
							FROM @tblQueryDetail
							WHERE Name = @crntMainTbl)

	DECLARE relationCrs CURSOR FOR
	SELECT *
		FROM @tblTableRelation
		WHERE TableName = @crntMainTbl
			OR ReferenceTableName = @crntMainTbl

	OPEN relationCrs
		FETCH NEXT FROM relationCrs INTO @fstTblNm, @fstTblClmNm, @refTblNm, @refTblClmNm
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF(@fstTblNm = @crntMainTbl)
			BEGIN
				UPDATE @tblQueryDetail 
					SET FirstTableName = @fstTblNm, FirstTableColumnName = @fstTblClmNm, RefTableName = @refTblNm, RefTableColumnName = @refTblClmNm, 
						Generation = @crntGeneration+1, HasRelation = (SELECT CASE WHEN(COUNT(TableName) > 0)
																					THEN(1) ELSE(0) END
																				FROM @tblTableRelation
																				WHERE ReferenceTableName = @refTblNm
																					OR TableName = @refTblNm) 
					WHERE (Name = @refTblNm AND IsChecked = 0)
			END
			ELSE IF(@refTblNm = @crntMainTbl)
			BEGIN
				UPDATE @tblQueryDetail 
					SET FirstTableName = @refTblNm, FirstTableColumnName = @refTblClmNm, RefTableName = @fstTblNm, RefTableColumnName = @fstTblClmNm, 
					Generation = @crntGeneration+1, HasRelation = (SELECT CASE WHEN(COUNT(TableName) > 0)
																					THEN(1) ELSE(0) END
																				FROM @tblTableRelation
																				WHERE ReferenceTableName = @fstTblNm
																					OR TableName = @fstTblNm)
					WHERE (Name = @fstTblNm AND IsChecked = 0)
			END


			FETCH NEXT FROM relationCrs INTO @fstTblNm, @fstTblClmNm, @refTblNm, @refTblClmNm
		END
	CLOSE relationCrs
	DEALLOCATE relationCrs


	UPDATE @tblQueryDetail
		SET IsChecked = 1
		WHERE Name = @crntMainTbl
END


--------------------------------- Show values -----------------------------------------------
--INSERT 
--	INTO @tblTableRelation (TableName, ColumnName, ReferenceTableName, ReferenceColumnName)
--	VALUES('ddf', 'fg', 'gh', 'hj')

--SELECT *
--	FROM @tblTableRelation
--SELECT *
--	FROM @tblQueryDetail
--	ORDER BY Generation
---------------------------------------------------------------------------------------------


-- Get the sql
DECLARE @hasRln INT
DECLARE @isChecked INT
DECLARE @sql VARCHAR(1000)
SET @sql = '';

-- Select rows where Generration is not null(if null means it is not in relation)
DECLARE sqlCrs CURSOR FOR
	SELECT *
		FROM @tblQueryDetail
		WHERE Generation IS NOT NULL
		   AND HasRelation = 1
		   AND IsChecked = 1
		ORDER BY Generation

OPEN sqlCrs
FETCH NEXT FROM sqlCrs INTO  @crntMainTbl , @crntGeneration, @hasRln, @isChecked, @fstTblNm, @fstTblClmNm, @refTblNm, @refTblClmNm
WHILE @@FETCH_STATUS = 0
BEGIN
	IF(@crntGeneration = 0)
	BEGIN
		SET @sql = @sql +'SELECT * FROM ' +@crntMainTbl;
	END
	ELSE IF(@crntGeneration > 0)
	BEGIN
		SET @sql = @sql +' INNER JOIN ' +@crntMainTbl +' ON ' +@fstTblNm +'.' +@fstTblClmNm +' = ' +@refTblNm +'.' +@refTblClmNm;
	END
	FETCH NEXT FROM sqlCrs INTO  @crntMainTbl , @crntGeneration, @hasRln, @isChecked, @fstTblNm, @fstTblClmNm, @refTblNm, @refTblClmNm
END
CLOSE sqlCrs
DEALLOCATE sqlCrs

--PRINT(@sql)
EXEC(@sql)

--------------------------------- Show values ----------------------------------------------
--target sql
SELECT *
	FROM Student
	INNER JOIN Department ON Student.Department_Id = Department.Id
	INNER JOIN Student_Course_Link ON Student.Id = Student_Course_Link.Student_Id
	INNER JOIN Course ON Student_Course_Link.Course_Id = Course.Id
	INNER JOIN University ON Department.University_id = University.Id	
-------------------------------------------------------------------------------------------- 
