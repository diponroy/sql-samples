------------- Student detail table =>

DECLARE @tblStudentdetail TABLE(Id BIGINT IDENTITY(1, 1) NOT NULL,
						        UniversityName NVARCHAR(100) NOT NULL,
								DepartmentName NVARCHAR(100) NOT NULL,
								StudentName NVARCHAR(100) NOT NULL,
								RowVersion TIMESTAMP)

INSERT INTO @tblStudentdetail(UniversityName, DepartmentName, StudentName)
	OUTPUT INSERTED.Id, INSERTED.UniversityName, INSERTED.DepartmentName, INSERTED.StudentName, CONVERT(BIGINT, INSERTED.RowVersion) AS RowVersion
	VALUES('SUST', 'CSE', 'Arman'),
		  ('SUST', 'CSE', 'Dipon'),
		  ('SUST', 'ARC', 'Rizvi'),
		  ('SUST', 'EEE', 'Rokon'),
		  ('AIUB', 'EEE', 'Arman'),
		  ('AIUB', 'CSE', 'Kamal'),
		  ('AIUB', 'ARC', 'Ripon')
------------- Student detail table <=



------------------ Lets test some thing =>

DECLARE @vrUniversityName VARCHAR(100)
DECLARE @vrDepartmentName VARCHAR(100)
DECLARE @vrStudentName VARCHAR(100)


----------- Test Case 1 =>
--SET @vrUniversityName = '';
--SET @vrDepartmentName = '';
--SET @vrStudentName = '';

----------- Test Case 2 =>
--SET @vrUniversityName = 'SUST';
--SET @vrDepartmentName = '';
--SET @vrStudentName = '';

----------- Test Case 3 =>
--SET @vrUniversityName = '';
--SET @vrDepartmentName = 'CSE';
--SET @vrStudentName = '';

----------- Test Case 4 =>
--SET @vrUniversityName = '';
--SET @vrDepartmentName = '';
--SET @vrStudentName = 'Arman';


----------- Test Case 5 =>
--SET @vrUniversityName = 'SUST';
--SET @vrDepartmentName = 'CSE';
--SET @vrStudentName = '';


--------- Test Case 6 =>
SET @vrUniversityName = 'SUST';
SET @vrDepartmentName = 'CSE';
SET @vrStudentName = 'Dipon';


SELECT *
	FROM @tblStudentdetail
	WHERE ((UniversityName = @vrUniversityName AND @vrUniversityName <> '') OR (@vrUniversityName = ''))
		AND ((DepartmentName = @vrDepartmentName AND @vrDepartmentName <> '') OR (@vrDepartmentName = ''))
		AND ((StudentName = @vrStudentName AND @vrStudentName <> '') OR (@vrStudentName = ''))


/*
* Test Case 1 => Seltcts All
*
* Test Case 2 => Seltcts WHERE university = 'SUST'
* Test Case 3 => Seltcts WHERE department = 'CSE'
* Test Case 4 => Seltcts WHERE stuentName = 'Arman'
*
* Test Case 5 => Seltcts WHERE university = 'SUST' AND department = 'CSE'
*
* Test Case 6 => Seltcts WHERE university = 'SUST' AND department = 'CSE' AND studentName = 'Dipon'
*/

-------------------- Six different querys took please at a single one -----------------





-------------- Ok, but whats about the Like Search !!! =>

--------- Test Case 7 =>
SET @vrUniversityName = 'SUST';
SET @vrDepartmentName = '';
SET @vrStudentName = 'z';

SELECT *
	FROM @tblStudentdetail
	WHERE ((UniversityName = @vrUniversityName AND @vrUniversityName <> '') OR (@vrUniversityName = ''))
		AND ((DepartmentName = @vrDepartmentName AND @vrDepartmentName <> '') OR (@vrDepartmentName = ''))
		AND ((StudentName LIKE '%' +@vrStudentName +'%' AND @vrStudentName <> '') OR (@vrStudentName = ''))
/*
* Test Case 7 => Seltcts Where university='Sust' And stuntName contains 'z'
*/

-------------------- Like serch even works just fine ;)

