--SELECT *
--	FROM LPurchaseIn
--	WHERE CAST(LpcTotalAmount as varchar) like '%2.75%'
	
	
--SELECT COLUMN_NAME, CAST(DATA_TYPE AS VARCHAR) AS DATA_TYPE
--	FROM INFORMATION_SCHEMA.COLUMNS
--	WHERE TABLE_NAME = 'LPurchaseIn'
	
	
	
	
----------------------------------
-- use
-- INSERT TABLE NAME --
-- INSERT LIKE STATMENT --
----------------------------------
DECLARE @tblName VARCHAR(100)
DECLARE @likeString VARCHAR(100)
DECLARE @columnName VARCHAR(100)
DECLARE @columnType VARCHAR(100)

DECLARE @cmd VARCHAR(8000)
DECLARE @where VARCHAR(8000)

-- INSERT TABLE NAME --
SET @tblName = 'LPurchaseIn'
-- INSERT LIKE STATMENT --
SET @likeString = '''%2%'''
SET @cmd = 'SELECT * FROM '+@tblName +' WHERE'
SET @where = ''

DECLARE	columnCursor CURSOR FOR
	SELECT COLUMN_NAME, CAST(DATA_TYPE AS VARCHAR) AS DATA_TYPE
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_NAME = @tblName
	
OPEN columnCursor   
   FETCH NEXT FROM columnCursor INTO @columnName, @columnType
   WHILE @@FETCH_STATUS = 0   
   BEGIN   
       IF @where != ''
			SET @where = @where + ' OR'
           
	   IF @columnType != 'datetime'
			SET @where = @where + ' ' + 'CAST(' +@tblName+'.'+@columnName +' AS VARCHAR)' + ' LIKE ' + @likeString
	   ELSE
			SET @where = @where + ' ' + 'CONVERT(VARCHAR,' +@tblName+'.'+@columnName+', 121)' + ' LIKE ' + @likeString  
       FETCH NEXT FROM columnCursor INTO @columnName, @columnType   
   END   
CLOSE columnCursor   
DEALLOCATE columnCursor 

	SET @cmd = @cmd + @where 
	--PRINT @cmd
	EXEC (@cmd)  
