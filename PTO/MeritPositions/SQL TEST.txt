USE [TestDB]
GO
/****** Object:  Table [dbo].[tblStudent]    Script Date: 12/20/2012 14:03:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStudent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Marke] [float] NOT NULL,
 CONSTRAINT [PK_tblStudent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tblStudent] ON
INSERT [dbo].[tblStudent] ([Id], [Name], [Marke]) VALUES (1, N'Dip', 70)
INSERT [dbo].[tblStudent] ([Id], [Name], [Marke]) VALUES (2, N'Rip', 70)
INSERT [dbo].[tblStudent] ([Id], [Name], [Marke]) VALUES (3, N'Kip', 80)
INSERT [dbo].[tblStudent] ([Id], [Name], [Marke]) VALUES (4, N'Ship', 80)
INSERT [dbo].[tblStudent] ([Id], [Name], [Marke]) VALUES (5, N'Kamal', 60)
INSERT [dbo].[tblStudent] ([Id], [Name], [Marke]) VALUES (6, N'Jamal', 60)
SET IDENTITY_INSERT [dbo].[tblStudent] OFF





















SELECT *
	FROM tblStudent

SELECT ROW_NUMBER()Over(ORDER BY Marke DESC) AS MeritPosition, Marke AS MeritPositionMark
	FROM tblStudent
	GROUP BY Marke
	
--------------------- Main --------------
DECLARE @position INT
	SET @position = 3;
	
WITH tblMerit AS
(
	SELECT ROW_NUMBER()Over(ORDER BY Marke DESC) AS MeritPosition, Marke AS MeritPositionMark
	FROM tblStudent
	GROUP BY Marke
 )
 
 SELECT tblStudent.Id, tblStudent.Name, tblStudent.Marke, tblMerit.MeritPosition
	FROM tblStudent
	LEFT JOIN tblMerit 
		ON tblStudent.Marke = tblMerit.MeritPositionMark
	--WHERE tblMerit.MeritPosition = @position
	
------------------------ END ----------------------------